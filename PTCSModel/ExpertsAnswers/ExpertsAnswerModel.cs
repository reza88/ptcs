﻿using System;

namespace PTCSModel.ExpertsAnswers
{
    public class ExpertsAnswerModel
    {
        public int QuestionnaireDimensionAutoKey { get; set; }
        public int ComponentAutoKey { get; set; }
        public Guid? QuestionnaireDimensionId { get; set; }
        public string QuestionnaireDimensionName { get; set; }
        public string ComponentName { get; set; }
        public string ComponentDescription { get; set; }
        public decimal ComponentWeight { get; set; }
        public bool IsDominant { get; set; }
        public byte? Opinion { get; set; }
    }
}
