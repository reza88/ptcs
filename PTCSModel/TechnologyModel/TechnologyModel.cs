﻿using System;

namespace PTCSModel.TechnologyModel
{
    public class AllComponent
    {
        public int Autokey { get; set; }
        public Guid? TechnologyId { get; set; }
        public Guid? QuestionnaireDimensionId { get; set; }
        public string QuestionnaireDimensionName { get; set; }
        public Guid? ComponentId { get; set; }
        public string ComponentName { get; set; }
        public string ComponentDescription { get; set; }
        public decimal? ComponentWeight { get; set; }
        public bool? IsDominant { get; set; }
    }
}
