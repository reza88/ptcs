﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PTCSModel.Models
{
    public class ExpertModel
    {
        public Guid? ExpertId { get; set; }
        public string ExpertName { get; set; }
    }
}
