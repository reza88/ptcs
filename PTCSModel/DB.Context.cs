﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PTCSModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PTCSEntities : DbContext
    {
        public PTCSEntities()
            : base("name=PTCSEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Tbl_Component> Tbl_Component { get; set; }
        public DbSet<Tbl_ExpertOpinion> Tbl_ExpertOpinion { get; set; }
        public DbSet<Tbl_ExpertOpinionDetail> Tbl_ExpertOpinionDetail { get; set; }
        public DbSet<Tbl_Parameters> Tbl_Parameters { get; set; }
        public DbSet<Tbl_QuestionnaireDimension> Tbl_QuestionnaireDimension { get; set; }
        public DbSet<Tbl_Senario> Tbl_Senario { get; set; }
        public DbSet<Tbl_SenarioDetail> Tbl_SenarioDetail { get; set; }
        public DbSet<Tbl_Typist> Tbl_Typist { get; set; }
        public DbSet<Tbl_Expert> Tbl_Expert { get; set; }
        public DbSet<Tbl_DefaultComponent> Tbl_DefaultComponent { get; set; }
        public DbSet<Tbl_ApplicationItem> Tbl_ApplicationItem { get; set; }
        public DbSet<Tbl_Technology> Tbl_Technology { get; set; }
    }
}
