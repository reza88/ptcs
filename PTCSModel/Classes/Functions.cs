﻿namespace PTCSModel.Classes
{
    public static class Functions
    {
        public static string GetConnectString(string filePath, string password)
        {
            return string.Format("DataSource={0}; Password='{1}'", filePath, password);
        }
    }
}
