﻿using System;

namespace PTCSModel.Senarios
{
    public class AllSenarioDetail
    {
        public Guid? SenarioId { get; set; }
        public Guid? TechnologyId { get; set; }
        public string SenarioName { get; set; }
        public double TechnicalSpecifications { get; set; }
        public double FinancialAndEconomicData { get; set; }
        public double MarketSpecifications { get; set; }
        public double RulesAndApprovals { get; set; }
        public decimal? Result { get; set; }
        public string ResultString { get; set; }
        public string Status { get; set; }
        public bool? IsMain { get; set; }
        public Guid? SenarioDetailId { get; set; }
    }
}
