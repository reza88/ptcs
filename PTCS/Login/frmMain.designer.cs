﻿namespace PTCS.Login
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnTechnology = new DevExpress.XtraEditors.SimpleButton();
            this.button2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnTypist = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuestionnaireDimension = new DevExpress.XtraEditors.SimpleButton();
            this.btnParameter = new DevExpress.XtraEditors.SimpleButton();
            this.btnExpert = new DevExpress.XtraEditors.SimpleButton();
            this.button7 = new DevExpress.XtraEditors.SimpleButton();
            this.button8 = new DevExpress.XtraEditors.SimpleButton();
            this.btnExpertOpinion = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnTechnology
            // 
            this.btnTechnology.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnTechnology.Appearance.Options.UseFont = true;
            this.btnTechnology.Location = new System.Drawing.Point(750, 138);
            this.btnTechnology.Name = "btnTechnology";
            this.btnTechnology.Size = new System.Drawing.Size(246, 48);
            this.btnTechnology.TabIndex = 3;
            this.btnTechnology.Text = "فن آوری";
            this.btnTechnology.Visible = false;
            this.btnTechnology.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button2.Appearance.Options.UseFont = true;
            this.button2.Location = new System.Drawing.Point(1002, 203);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(246, 59);
            this.button2.TabIndex = 1;
            this.button2.Text = "تنظیمات";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnTypist
            // 
            this.btnTypist.Appearance.Font = new System.Drawing.Font("Iranian Sans", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnTypist.Appearance.Options.UseFont = true;
            this.btnTypist.Location = new System.Drawing.Point(751, 203);
            this.btnTypist.Name = "btnTypist";
            this.btnTypist.Size = new System.Drawing.Size(245, 45);
            this.btnTypist.TabIndex = 6;
            this.btnTypist.Text = "کاربر";
            this.btnTypist.Visible = false;
            this.btnTypist.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnQuestionnaireDimension
            // 
            this.btnQuestionnaireDimension.Appearance.Font = new System.Drawing.Font("Iranian Sans", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnQuestionnaireDimension.Appearance.Options.UseFont = true;
            this.btnQuestionnaireDimension.Location = new System.Drawing.Point(751, 254);
            this.btnQuestionnaireDimension.Name = "btnQuestionnaireDimension";
            this.btnQuestionnaireDimension.Size = new System.Drawing.Size(245, 45);
            this.btnQuestionnaireDimension.TabIndex = 7;
            this.btnQuestionnaireDimension.Text = " ابعاد پرسشنامه";
            this.btnQuestionnaireDimension.Visible = false;
            this.btnQuestionnaireDimension.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnParameter
            // 
            this.btnParameter.Appearance.Font = new System.Drawing.Font("Iranian Sans", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnParameter.Appearance.Options.UseFont = true;
            this.btnParameter.Location = new System.Drawing.Point(751, 305);
            this.btnParameter.Name = "btnParameter";
            this.btnParameter.Size = new System.Drawing.Size(245, 45);
            this.btnParameter.TabIndex = 8;
            this.btnParameter.Text = "تنظیمات پایه";
            this.btnParameter.Visible = false;
            this.btnParameter.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnExpert
            // 
            this.btnExpert.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExpert.Appearance.Options.UseFont = true;
            this.btnExpert.Location = new System.Drawing.Point(750, 192);
            this.btnExpert.Name = "btnExpert";
            this.btnExpert.Size = new System.Drawing.Size(246, 48);
            this.btnExpert.TabIndex = 4;
            this.btnExpert.Text = "خبرگان";
            this.btnExpert.Visible = false;
            this.btnExpert.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button7.Appearance.Options.UseFont = true;
            this.button7.Location = new System.Drawing.Point(72, 654);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(203, 59);
            this.button7.TabIndex = 9;
            this.button7.Text = "خروج";
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button8.Appearance.Options.UseFont = true;
            this.button8.Location = new System.Drawing.Point(1002, 268);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(246, 59);
            this.button8.TabIndex = 2;
            this.button8.Text = "خروجی سیستم";
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnExpertOpinion
            // 
            this.btnExpertOpinion.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExpertOpinion.Appearance.Options.UseFont = true;
            this.btnExpertOpinion.Location = new System.Drawing.Point(750, 246);
            this.btnExpertOpinion.Name = "btnExpertOpinion";
            this.btnExpertOpinion.Size = new System.Drawing.Size(246, 48);
            this.btnExpertOpinion.TabIndex = 5;
            this.btnExpertOpinion.Text = "پاسخ خبرگان";
            this.btnExpertOpinion.Visible = false;
            this.btnExpertOpinion.Click += new System.EventHandler(this.button9_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Iranian Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(1002, 138);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(246, 59);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "منوی اصلی";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(91)))), ((int)(((byte)(73)))));
            this.BackgroundImage = global::PTCS.Properties.Resources.Amazing_Wallpaper_Technology;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1260, 745);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnExpertOpinion);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.btnExpert);
            this.Controls.Add(this.btnParameter);
            this.Controls.Add(this.btnQuestionnaireDimension);
            this.Controls.Add(this.btnTypist);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnTechnology);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Click += new System.EventHandler(this.frmMain_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnTechnology;
        private DevExpress.XtraEditors.SimpleButton button2;
        private DevExpress.XtraEditors.SimpleButton btnTypist;
        private DevExpress.XtraEditors.SimpleButton btnQuestionnaireDimension;
        private DevExpress.XtraEditors.SimpleButton btnParameter;
        private DevExpress.XtraEditors.SimpleButton btnExpert;
        private DevExpress.XtraEditors.SimpleButton button7;
        private DevExpress.XtraEditors.SimpleButton button8;
        private DevExpress.XtraEditors.SimpleButton btnExpertOpinion;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}