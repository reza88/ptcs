﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using PTCS.Forms.Expert;
using PTCS.Forms.ExpertsAnswer;
using PTCS.Forms.Parameters;
using PTCS.Forms.QuestionnaireDimension;
using PTCS.Forms.Senario;
using PTCS.Forms.Technology;
using PTCS.Forms.Typist;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Login
{
    public partial class FrmMain : Form
    {
        int _cNbTypist;
        public PTCSEntities PtcsEntities;
        private Tbl_Typist _tblTypist;
        public FrmMain(int nbTypist)
        {

            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            _tblTypist = new Tbl_Typist();
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;

            InitializeComponent();

            _cNbTypist = nbTypist;
        }

       
        private bool _perTechnology;
        private bool _perExpert;
        private bool _perExpertOpinion;
        private bool _perTypist;
        private bool _perQuestionnaireDimension;
        private bool _perParameter;
        private bool _perFinalResult;

        private void button7_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
        var permission = PtcsEntities.Tbl_Typist
                .Where(i => i.nb == _cNbTypist)
                .Select(i => i.access_inprog)
                .FirstOrDefault();

            if (permission != null)
            {
                if ((permission.Contains(",1-1,") && (permission.IndexOf(",1-1,") == 0 || permission.Substring(permission.IndexOf(",1-1,") - 1, 1) != "-")) || permission.Contains(",1-1,") || permission.StartsWith("1-1,"))
                {
                    _perTechnology = true;
                }
                else
                {
                    _perTechnology = false;
                }


                if ((permission.Contains(",1-2,") && (permission.IndexOf(",1-2,") == 0 || permission.Substring(permission.IndexOf(",1-2,") - 1, 1) != "-")) || permission.Contains(",1-2,") || permission.StartsWith("1-2,"))
                {
                    _perExpert = true;
                }
                else
                {
                    _perExpert = false;
                }

                if ((permission.Contains(",1-3,") && (permission.IndexOf(",1-3,") == 0 || permission.Substring(permission.IndexOf(",1-3,") - 1, 1) != "-")) || permission.Contains(",1-3,") || permission.StartsWith("1-3,"))
                {
                    _perExpertOpinion = true;
                }
                else
                {
                    _perExpertOpinion = false;
                }

                if ((permission.Contains(",2-1,") && (permission.IndexOf(",2-1,") == 0 || permission.Substring(permission.IndexOf(",2-1,") - 1, 1) != "-")) || permission.Contains(",2-1,") || permission.StartsWith("2-1,"))
                {
                    _perTypist = true;
                }
                else
                {
                    _perTypist = false;
                }

                if ((permission.Contains(",2-2,") && (permission.IndexOf(",2-2,") == 0 || permission.Substring(permission.IndexOf(",2-2,") - 1, 1) != "-")) || permission.Contains(",2-2,") || permission.StartsWith("2-2,"))
                {
                    _perQuestionnaireDimension = true;
                }
                else
                {
                    _perQuestionnaireDimension = false;
                }

                if ((permission.Contains(",2-3,") && (permission.IndexOf(",2-3,") == 0 || permission.Substring(permission.IndexOf(",2-3,") - 1, 1) != "-")) || permission.Contains(",2-3,") || permission.StartsWith("2-3,"))
                {
                    _perParameter = true;
                }
                else
                {
                    _perParameter = false;
                }


                if ((permission.Contains(",3,") && (permission.IndexOf(",3,") == 0 || permission.Substring(permission.IndexOf(",3,") - 1, 1) != "-")) || permission.Contains(",3,") || permission.StartsWith("3,"))
                {
                    _perFinalResult = true;
                }
                else
                {
                    _perFinalResult = false;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (_perTypist)
            {
                Frmtypist frm = new Frmtypist(_cNbTypist);
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به اطلاعات کاربر را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_perTechnology)
            {
                FrmTechnology frm = new FrmTechnology(_cNbTypist);
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به اطلاعات فن آوری را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (_perExpert)
            {
                FrmExpert frm = new FrmExpert(_cNbTypist);
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به اطلاعات خبرگان را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (_perQuestionnaireDimension)
            {
                FrmQuestionnaireDimension frm = new FrmQuestionnaireDimension();
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به اطلاعات ابعاد پرسشنامه را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (_perParameter)
            {
                FrmParameters frm = new FrmParameters();
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به تنظیمات برنامه را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (_perExpertOpinion)
            {
                FrmExpertsAnswer frm = new FrmExpertsAnswer(_cNbTypist);
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به اطلاعات ابعاد پرسشنامه را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (_perFinalResult)
            {
                FrmSenario frm = new FrmSenario(_cNbTypist);
                frm.ShowDialog();
            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز دسترسی به اطلاعات خروجی برنامه را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void frmMain_Click(object sender, EventArgs e)
        {
            btnTechnology.Visible = false;
            btnExpert.Visible = false;
            btnExpertOpinion.Visible = false;

            btnTypist.Visible = false;
            btnParameter.Visible = false;
            btnQuestionnaireDimension.Visible = false;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            btnTechnology.Visible = true;
            btnExpert.Visible = true;
            btnExpertOpinion.Visible = true;

            btnTypist.Visible = false;
            btnParameter.Visible = false;
            btnQuestionnaireDimension.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            btnTechnology.Visible = false;
            btnExpert.Visible = false;
            btnExpertOpinion.Visible = false;

            btnTypist.Visible = true;
            btnParameter.Visible = true;
            btnQuestionnaireDimension.Visible = true;
        }
    }
}
