﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Login
{
    public partial class FrmLogin : Form
    {
        private int _cCountExit = 0;
        public PTCSEntities PtcsEntities;
        
        public FrmLogin()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
            InitializeComponent();
        }


        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{tab}");
            if (e.KeyCode == Keys.Right)
                SendKeys.Send("+{tab}");
        }

        private string Hashing(string n)
        {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] bytes = ue.GetBytes(n);

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            
            byte[] hashBytes = md5.ComputeHash(bytes);

            // Bytes to string

            return System.Text.RegularExpressions.Regex.Replace
            (BitConverter.ToString(hashBytes), "-", "").ToLower();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                picEnter_Click(sender,e);
            }
        }

        private void picEnter_Click(object sender, EventArgs e)
        {
            if (txtUser.Text != "" && txtPass.Text != "")
            {
                var user = Hashing(txtUser.Text.Trim());
                var pass = Hashing(txtPass.Text.Trim());
                var query = PtcsEntities.Tbl_Typist
                    .Where(i => i.username == user &&
                                i.password == pass &&
                                i.active)
                    .Select(i => i)
                    .FirstOrDefault();

                if (query!=null)
                {

                    FrmMain frmMain = new FrmMain(query.nb);
                    txtUser.Text = "";
                    txtPass.Text = "";
                    this.Hide();
                    frmMain.ShowDialog();
                    this.Show();
                    txtUser.Focus();
                   
                }
                else
                {
                    MessageBox.Show("نام کاربری یا گذر واژه اشتباه است", "آگهی");
                    _cCountExit++;
                    if (_cCountExit < 3)
                    {
                        txtPass.Text = "";
                        txtUser.Focus();
                    }
                    else
                        Close();
                }
            }
            else
                MessageBox.Show("نام کاربری و گذر واژه را وارد کنید", "آگهی");

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.WindowState = FormWindowState.Normal;
        }
    }
}
