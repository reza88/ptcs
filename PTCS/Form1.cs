﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PTCS
{
    public partial class Form100 : Form
    {
        public Form100()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<Tuple<int,string>> student=new List<Tuple<int, string>>();

            student.Add(new Tuple<int, string>(1,"ali"));
            student.Add(new Tuple<int, string>(2,"reza"));
            student.Add(new Tuple<int, string>(3,"Mohammad"));
            student.Add(new Tuple<int, string>(4,"mina"));
            student.Add(new Tuple<int, string>(5,"sara"));

            dataGridView1.DataSource = student;



            for (int i = 0; i < student.Count(); i++)
            {
                int n = dataGridView2.Rows.Add();

                dataGridView2.Rows[n].Cells[0].Value = student[i].Item1;
                dataGridView2.Rows[n].Cells[1].Value = student[i].Item2;
            }
        }
    }
}
