﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.Parameters
{
    public partial class FrmParameters : XtraForm
    {
        public PTCSEntities PtcsEntities;
        public FrmParameters()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
            InitializeComponent();
        }



        private void FrmParameters_Load(object sender, EventArgs e)
        {
          var query = PtcsEntities.Tbl_Parameters
                .Select(i => i)
                .ToList();

            if (query.Count() != 0)
            {
                foreach (var rec in query)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = rec.nbe;
                    dataGridView1.Rows[n].Cells[1].Value = rec.@object;
                    dataGridView1.Rows[n].Cells[2].Value = rec.data_variable;
                }
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (dataGridView1.RowCount > 0)
                {
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                       var parameterNb = Convert.ToInt32(dataGridView1.Rows[i].Cells[0].Value.ToString());
                        var query = PtcsEntities.Tbl_Parameters
                            .Where(a => a.nbe == parameterNb &&
                                        a.name_variable!= "Low threshold for dominant components")
                            .Select(a => a)
                            .FirstOrDefault();

                        if (query!= null)
                        {
                            query.data_variable = dataGridView1.Rows[i].Cells[2].Value != null ? dataGridView1.Rows[i].Cells[2].Value.ToString() : "";
                        }

                        PtcsEntities.SaveChanges();


                    }
                    XtraMessageBox.Show("تنظیمات برنامه با موفقیت ویرایش شد", "آگهی", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                XtraMessageBox.Show("مشکل در ثبت تنظیمات برنامه", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
