﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;
using PTCSModel.Models;

namespace PTCS.Forms.Expert
{
    public partial class FrmExpert : XtraForm
    {
        readonly int _cNbTypist;
        public Guid ExpertId { get; set; }

        //public string record_active;
        private Boolean _varAdding;
        private Boolean _varEditing;
        private int _autoKey;

        public PTCSEntities PtcsEntities;
        private Tbl_Expert _tblExpert;
        public FrmExpert(int nbTypist)
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            _tblExpert = new Tbl_Expert();
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;

            SkinManager.EnableFormSkins();
            _cNbTypist = nbTypist;
            InitializeComponent();
        }



        private void frmExpert_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo language = new System.Globalization.CultureInfo("fa-IR");
            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(language);

            proc_EnDis();

            ViewExpert vw = new ViewExpert();

            ExpertId = vw.GetLast();

            if (ExpertId != null)
                proc_refresh();
        }



        private void btnNew_Click(object sender, EventArgs e)
        {
            _varAdding = true;

            proc_empty();

            proc_EnDis();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            //var db = new DataClasses1DataContext();
            //var query = from i in db.Typists
            //            where i.nb==(_cNbTypist)
            //            select i.access_inprog;
            var query = from i in PtcsEntities.Tbl_Typist
                        where i.nb == _cNbTypist
                        select i.access_inprog;
            if (query.Count() != 0)
            {
                var firstOrDefault = query.FirstOrDefault();
                if (firstOrDefault != null && (firstOrDefault.Contains(",1-2-1,") || firstOrDefault.Contains("1-2-1,")))
                {
                    _varEditing = true;
                    proc_EnDis();
                    txtExpertName.Focus();
                    //record_active = txtNb.Text;
                }
                else
                    XtraMessageBox.Show("کاربر محترم شما مجوز ویرایش اطلاعات خبره را ندارید", "آگهی",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtExpertName.Text != string.Empty)
            {
                ExpertModel expertModel;
                if (_varAdding && _varEditing == false)
                {
                    bool insertDuplicateExists = PtcsEntities.Tbl_Expert.Any(x => x.ExpertName == txtExpertName.Text.Trim());
                    //var _exit = (from i in db.Technologies
                    //              where i.TechnologyName==(txtTechnologyName.Text) 
                    //              select i).FirstOrDefault();
                    // if (_exit == null)
                    // //if (!duplicateExists)
                    // {

                    expertModel = new ExpertModel
                    {
                        ExpertId = ExpertId,
                        ExpertName = txtExpertName.Text.Trim()
                    };
                    if (!insertDuplicateExists)
                    {
                        var res = PtcsEntities.Tbl_Expert.Add(new Tbl_Expert
                        {
                            ExpertId = Guid.NewGuid(),
                            ExpertName = txtExpertName.Text.Trim(),
                            Active = true
                        });

                        if (res != null && res.ExpertId != Guid.Empty)
                        {
                            PtcsEntities.SaveChanges();
                            ExpertId = res.ExpertId;
                            proc_refresh();
                        }
                        else
                            XtraMessageBox.Show("خطا در ثبت اطلاعات خبره", "خطا", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    }
                    else
                        XtraMessageBox.Show(" اطلاعات خبره تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                    //var result = db.Expert_psave(txtExpertName.Text.Trim()).FirstOrDefault();

                    //if (result != null && result.InsertStatus != -1)
                    //{
                    //    ExpertId = Guid.Parse(result.Id);
                    //    proc_refresh();
                    //}
                    //else
                    //    XtraMessageBox.Show("خطا در ثبت اطلاعات خبره", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    _varAdding = false;
                    _varEditing = false;
                    proc_EnDis();


                }


                else if (_varAdding == false && _varEditing)
                {
                    bool duplicateExists = PtcsEntities.Tbl_Expert.Any(x => x.ExpertId != ExpertId && x.ExpertName == txtExpertName.Text.Trim());

                    if (!duplicateExists)
                    {



                        var updateQuery = PtcsEntities.Tbl_Expert
                                        .FirstOrDefault(item => item.ExpertId == ExpertId);
                        if (updateQuery != null)
                        {

                            updateQuery.ExpertName = txtExpertName.Text.Trim();
                            updateQuery.Active = rdActive1.Checked;

                            PtcsEntities.SaveChanges();

                            _varAdding = false;
                            _varEditing = false;
                            proc_EnDis();
                            proc_refresh();
                        }
                    }

                    else
                        XtraMessageBox.Show(" اطلاعات خبره تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                }
            }
            else
                XtraMessageBox.Show("اطلاعات را کامل وارد کنید", "اخطار");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _varAdding = false;
            _varEditing = false;
            proc_EnDis();
            if (ExpertId != null)
                proc_refresh();


        }

        private void frmExpert_KeyDown(object sender, KeyEventArgs e)
        {
            ViewExpert vw = new ViewExpert();


            if (!_varAdding && !_varEditing)
            {

                if (e.KeyCode == Keys.F7)
                {
                    btnSearch_Click(sender, e);
                }
                #region previous


                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.None)
                {

                    ExpertId = vw.GetPrevious(_autoKey);

                    if (ExpertId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("اولین پرونده است");
                }

                #endregion

                #region Next

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.None)
                {

                    ExpertId = vw.GetNext(_autoKey);

                    if (ExpertId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("آخرین پرونده است");

                }

                #endregion

                #region First

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.Control)
                {
                    ExpertId = vw.GetFirst();

                    if (ExpertId != null)
                        proc_refresh();
                }

                #endregion

                #region Last

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Control)
                {
                    ExpertId = vw.GetLast();

                    if (ExpertId != null)
                        proc_refresh();

                }

                #endregion




            }
        }

        private void txtTechnologyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{tab}");
            if (e.KeyCode == Keys.Right)
                SendKeys.Send("+{tab}");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void proc_empty()
        {
            txtExpertName.Text = "";
            rdActive1.Checked = true;

            txtExpertName.Focus();
        }

        public void proc_EnDis()
        {
            if (_varAdding)
            {
                txtExpertName.Enabled = true;
                groupBox1.Enabled = true;

                btnNew.Enabled = false;
                btnEdit.Enabled = false;
                btnExit.Enabled = false;
                btnSearch.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;

            }
            else if (_varEditing)
            {
                txtExpertName.Enabled = true;
                groupBox1.Enabled = true;

                btnNew.Enabled = false;
                btnEdit.Enabled = false;
                btnExit.Enabled = false;
                btnSearch.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;
            }
            else
            {
                txtExpertName.Enabled = false;
                groupBox1.Enabled = false;

                btnNew.Enabled = true;
                btnEdit.Enabled = true;
                btnExit.Enabled = true;
                btnSearch.Enabled = true;
                btnSave.Enabled = false;
                btnCancel.Enabled = false;
            }
        }

        public void proc_refresh()
        {


            if (ExpertId != null)
            {

                var query = PtcsEntities.Tbl_Expert.FirstOrDefault(item => item.ExpertId == ExpertId);
                if (query != null)
                {

                    if (query != null)
                    {
                        _autoKey = query.AutoKey;

                        if (query.Active == true)
                        {
                            rdActive1.Checked = true;
                            rdActive2.Checked = false;
                        }
                        else
                        {
                            rdActive1.Checked = false;
                            rdActive2.Checked = true;
                        }

                        ExpertId = query.ExpertId;

                        txtExpertName.Text = query.ExpertName;
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FrmExpertSearch a = new FrmExpertSearch();

            a.ShowDialog();
            if (a.Ss != Guid.Empty)
            {
                ExpertId = a.Ss;
                proc_refresh();
            }
        }
    }
}
