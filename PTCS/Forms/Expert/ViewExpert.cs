﻿using System;
using System.Linq;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.Expert
{
    class ViewExpert
    {
        public PTCSEntities PtcsEntities;
        
        public ViewExpert()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");

            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
        }

        public Guid GetLast()
        {
           return PtcsEntities.Tbl_Expert
                .OrderByDescending(item => item.AutoKey)
                .Select(item => item.ExpertId).FirstOrDefault();
        }

        public Guid GetFirst()
        {
            return PtcsEntities.Tbl_Expert
                .OrderBy(item => item.AutoKey)
                .Select(item => item.ExpertId).FirstOrDefault();
        }

        public Guid GetPrevious(int autoKey)
        {
            return PtcsEntities.Tbl_Expert
                .Where(item => item.AutoKey < autoKey)
                .OrderByDescending(item => item.AutoKey)
                .Select(item => item.ExpertId).FirstOrDefault();
        }

        public Guid GetNext(int autoKey)
        {
            return PtcsEntities.Tbl_Expert
                .Where(item => item.AutoKey > autoKey)
                .OrderBy(item => item.AutoKey)
                .Select(item => item.ExpertId).FirstOrDefault();
        }

       




    }
}
