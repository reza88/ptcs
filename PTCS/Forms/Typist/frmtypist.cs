﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;
using ComboBox = System.Windows.Forms.ComboBox;

namespace PTCS.Forms.Typist
{
    public partial class Frmtypist : XtraForm
    {
        public bool VarAdding, VarEditing;
        public string RecordActive;
        public bool Active;
        private bool _fAcces, _varview;
        int _fsu, _ssu, _tsu;

        public int CNbTypist;
        public PTCSEntities PtcsEntities;
        private Tbl_Typist _tblTypist;
        public Frmtypist(int nbTypist)
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            _tblTypist = new Tbl_Typist();
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;

            InitializeComponent();

            CNbTypist = nbTypist;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            var query = (from i in PtcsEntities.Tbl_Typist
                         where i.nb == CNbTypist && i.access == "Superviser"
                         select i.name).FirstOrDefault();
            if (query != null)
            {
                FrmTypistSearch a = new FrmTypistSearch(CNbTypist);

                a.ShowDialog();
                if (!string.IsNullOrEmpty(a.Ss))
                {
                    RecordActive = a.Ss;
                    txtNb.Text = a.Ss;
                    proc_refresh();
                }
            }
        }

        private void proc_EnDis()
        {

            var query = PtcsEntities.Tbl_Typist
                .Where(i => i.nb == CNbTypist)
                .Select(i => i)
                .FirstOrDefault();

            if (query != null)
            {
                if (VarAdding)
                {

                    txtName.Enabled = true;
                    txtUser.Enabled = true;
                    txtPass.Enabled = true;
                    txtPost.Enabled = true;
                    txtDate_begin.Enabled = true;
                    txtDate_end.Enabled = true;
                    ck_active.Enabled = true;
                    groupBox1.Enabled = true;




                    btnCancel.Enabled = true;
                    btnSave.Enabled = true;

                    btnSearch.Enabled = false;
                    btnNew.Enabled = false;
                    btnEdit.Enabled = false;

                    btnExit.Enabled = false;
                    txtName.Focus();

                }
                else if (VarEditing)
                {
                    if (query.access == "Superviser")
                    {
                        txtName.Enabled = true;
                        txtUser.Enabled = true;
                        txtPass.Enabled = true;
                        txtPost.Enabled = true;
                        txtDate_begin.Enabled = true;
                        txtDate_end.Enabled = true;
                        ck_active.Enabled = true;
                        groupBox1.Enabled = true;



                        btnCancel.Enabled = true;
                        btnSave.Enabled = true;
                        btnSearch.Enabled = false;
                        btnNew.Enabled = false;
                        btnEdit.Enabled = false;

                        btnNew.Focus();
                    }
                    else
                    {
                        txtName.Enabled = true;
                        txtUser.Enabled = true;
                        txtPass.Enabled = true;
                        txtPost.Enabled = false;
                        txtDate_begin.Enabled = false;
                        txtDate_end.Enabled = false;
                        ck_active.Enabled = false;
                        groupBox1.Enabled = false;



                        btnCancel.Enabled = true;
                        btnSave.Enabled = true;
                        btnSearch.Enabled = false;
                        btnNew.Enabled = false;
                        btnEdit.Enabled = false;

                        btnNew.Focus();
                    }
                }

                else
                {
                    txtName.Enabled = false;
                    txtUser.Enabled = false;
                    txtPass.Enabled = false;
                    txtPost.Enabled = false;
                    txtDate_begin.Enabled = false;
                    txtDate_end.Enabled = false;
                    ck_active.Enabled = false;

                    groupBox1.Enabled = false;




                    btnCancel.Enabled = false;
                    btnSave.Enabled = false;
                    btnNew.Enabled = true;
                    btnEdit.Enabled = true;

                    btnSearch.Enabled = true;
                    btnExit.Enabled = true;
                    btnNew.Focus();

                }
            }
        }

        private int proc_strTOint32(string strVar)
        {
            int intVar;
            bool isNumeric = int.TryParse(strVar, out intVar);
            if (isNumeric)
                return intVar;
            else
                return 0;
        }

        private void proc_refresh()
        {
            var numberBase = Convert.ToInt32(txtNb.Text.Trim());
            var query = PtcsEntities.Tbl_Typist
               .Where(i => i.nb == numberBase)
               .Select(i => i)
               .FirstOrDefault();

            if (query != null)
            {
                txtNb.Text = query.nb.ToString();
                txtName.Text = query.name;
                //    txtUser.Text = query.username;
                //    txtPass.Text = query.password;
                txtPost.Text = query.post;
                txtDate_begin.Text = query.date_begin;
                txtDate_end.Text = query.date_end;

                if (query.active)
                {
                    rdActive1.Checked = true;
                    rdActive2.Checked = false;
                }
                else
                {
                    rdActive1.Checked = false;
                    rdActive2.Checked = true;
                }

            }

        }



        private void proc_viewaccess(string acces)
        {
            string[] gk = acces.Split(new Char[] { ',' });
            foreach (var k in gk)
            {
                string[] splite = k.Split(new char[] { '-' });
                switch (splite.Length)
                {
                    case 1:
                        if (trvAccessInprog.Nodes.IndexOfKey(k) >= 0)
                            trvAccessInprog.Nodes[k].Checked = true; break;
                    case 2:
                        if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(k) >= 0)
                            trvAccessInprog.Nodes[splite[0]].Nodes[k].Checked = true;
                        break;
                    case 3:
                        if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(splite[0] + "-" + splite[1]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes.IndexOfKey(k) >= 0)
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[k].Checked = true;
                        break;
                    case 4:
                        if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(splite[0] + "-" + splite[1]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes.IndexOfKey(splite[0] + "-" + splite[1] + "-" + splite[2]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes.IndexOfKey(k) >= 0)
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes[k].Checked = true;
                        break;
                    case 5:
                        if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(splite[0] + "-" + splite[1]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes.IndexOfKey(splite[0] + "-" + splite[1] + "-" + splite[2]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes.IndexOfKey(splite[0] + "-" + splite[1] + "-" + splite[2] + "-" + splite[3]) >= 0 &&
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2] + "-" + splite[3]].Nodes.IndexOfKey(k) >= 0)
                            trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2] + "-" + splite[3]].Nodes[k].Checked = true;
                        break;
                }
            }
        }

        private void proc_trvaccess_draw()
        {

            trvAccessInprog.Nodes.Clear();
            var gh = (from g in PtcsEntities.Tbl_ApplicationItem
                      select g).ToList();
            int max = 0;
            foreach (var p in gh)
            {
                int m2 = p.CodeItem.Split(new Char[] { '-' }).Length;
                if (max < m2)
                    max = m2;
            }
            for (int l = 1; l <= max; l++)
            {

                var w = from f in gh
                        where f.CodeItem.Trim().Split(new char[] { '-' }).Length == l
                        select f;
                foreach (var k in w)
                {
                    string[] splite = k.CodeItem.Trim().Split(new char[] { '-' });
                    switch (l)
                    {
                        case 1:
                            trvAccessInprog.Nodes.Add(k.CodeItem.Trim(), k.CaptionItem); break;
                        case 2:
                            if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0)
                                trvAccessInprog.Nodes[splite[0]].Nodes.Add(k.CodeItem.Trim(), k.CaptionItem);
                            break;
                        case 3:
                            if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                                trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(splite[0] + "-" + splite[1]) >= 0)
                                trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes.Add(k.CodeItem.Trim(), k.CaptionItem);
                            break;
                        case 4:
                            if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                                trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(splite[0] + "-" + splite[1]) >= 0 &&
                                trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes.IndexOfKey(splite[0] + "-" + splite[1] + "-" + splite[2]) >= 0)
                                trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes.Add(k.CodeItem.Trim(), k.CaptionItem);
                            break;
                        case 5:
                            if (trvAccessInprog.Nodes.IndexOfKey(splite[0]) >= 0 &&
                                trvAccessInprog.Nodes[splite[0]].Nodes.IndexOfKey(splite[0] + "-" + splite[1]) >= 0 &&
                                trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes.IndexOfKey(splite[0] + "-" + splite[1] + "-" + splite[2]) >= 0 &&
                                trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes.IndexOfKey(splite[0] + "-" + splite[1] + "-" + splite[2] + "-" + splite[3]) >= 0)
                                trvAccessInprog.Nodes[splite[0]].Nodes[splite[0] + "-" + splite[1]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2]].Nodes[splite[0] + "-" + splite[1] + "-" + splite[2] + "-" + splite[3]].Nodes.Add(k.CodeItem.Trim(), k.CaptionItem);
                            break;
                    }
                }
            }
        }


        private void frmEdit_typist_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo language = new System.Globalization.CultureInfo("fa-IR");
            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(language);

            proc_trvaccess_draw();

            proc_EnDis();



            var query = PtcsEntities.Tbl_Typist
               .Where(i => i.nb == CNbTypist)
               .Select(i => i)
               .FirstOrDefault();

            if (query != null)
            {
                txtNb.Text = query.nb.ToString();

                proc_refresh();
                proc_trv_empty(trvAccessInprog);
                proc_viewaccess(query.access_inprog);
            }
            //}

            if (txtNb.Text != "")
                _fAcces = true;
        }

        private void proc_empty()
        {

            foreach (var c in Controls)
            {
                if (c is TextBox)
                    (c as TextBox).Clear();
                if (c is MaskedTextBox)
                    (c as MaskedTextBox).Clear();
                if (c is RichTextBox)
                    (c as RichTextBox).Clear();

                if (c is ComboBox)
                {
                    (c as ComboBox).Text = string.Empty;
                }
                if (c is CheckBox)
                {
                    (c as CheckBox).Checked = false;
                }
                if (c is GroupBox)
                {
                    foreach (var d in (c as GroupBox).Controls)
                    {
                        //MessageBox.Show(d.ToString());

                        if (d is TextBox)
                            (d as TextBox).Clear();
                        if (d is MaskedTextBox)
                            (d as MaskedTextBox).Clear();
                        if (d is RichTextBox)
                            (d as RichTextBox).Clear();
                        if (d is ComboBox)
                        {
                            (d as ComboBox).Text = string.Empty;
                        }

                        ck_active.Checked = true;



                    }
                }
            }

            groupBox1.Enabled = true;



            //  lstFirst.SetSelected(0, true);
            rdActive1.Checked = true;


        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            var query = PtcsEntities.Tbl_Typist
               .Where(i => i.nb == CNbTypist)
               .Select(i => i)
               .FirstOrDefault();

            if (query != null)
            {
                if (query.access == "Superviser")
                {
                    VarAdding = true;

                    panel2.Visible = false;
                    if (txtNb.Text != string.Empty)
                        RecordActive = txtNb.Text;
                    else
                        RecordActive = "";

                    proc_empty();

                    proc_EnDis();

                    proc_trvaccess_draw();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            VarAdding = false;
            VarEditing = false;
            proc_EnDis();

            if (txtNb.Text.Trim() != "")
                proc_refresh();

            txtNb.Text = RecordActive;

            proc_refresh();

            var query = PtcsEntities.Tbl_Typist
              .Where(i => i.nb == CNbTypist)
              .Select(i => i)
              .FirstOrDefault();

            if (query != null)
            {
                proc_trv_empty(trvAccessInprog);
                proc_viewaccess(query.access_inprog);
            }
        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{tab}");
            }
            if (e.KeyCode == Keys.Right)
            {
                SendKeys.Send("+{tab}");
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (rdActive1.Checked)
                Active = true;
            else
                Active = false;



            if (VarAdding && VarEditing == false)
            {
                if (txtName.Text != "" && txtUser.Text != "" && txtPass.Text != "")//&& cmbSecond_SU.SelectedIndex!=-1 && cmbThird_SU.SelectedIndex!=-1)// && access_time != "")
                {
                    if (txtPass.Text.Length < 4)
                        MessageBox.Show("گذر واژه حداقل باید 4 کاراکتر باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        var userName = PtcsEntities.Tbl_Typist
                                          .Where(i => i.username == txtUser.Text.Trim())
                                          .Select(i => i)
                                          .FirstOrDefault();

                        if (userName != null)
                        {
                            MessageBox.Show("نام کاربری تکراری می باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            PtcsEntities.Tbl_Typist.Add(new Tbl_Typist
                            {
                                active = true,
                                name = txtName.Text.Trim(),
                                username = Hashing(txtUser.Text.Trim()),
                                password = Hashing(txtPass.Text.Trim()),
                                access = "",
                                access_inprog = "",
                                date_begin = txtDate_begin.Text.Trim(),
                                date_end = txtDate_end.Text.Trim(),
                                datetime_send = "",
                                post = txtPost.Text.Trim()

                            });
                            PtcsEntities.SaveChanges();
                            VarAdding = false;
                            VarEditing = false;
                            proc_EnDis();

                            var query = PtcsEntities.Tbl_Typist
                                .Select(i => i.nb)
                                .ToList().Max();
                            txtNb.Text = query.ToString();
                            proc_refresh();
                            _fAcces = true;
                            trvAccessInprog.Enabled = true;
                            trvAccessInprog.Focus();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("اطلاعات را کامل وارد کنید", "اخطار");
                }

            }

            else if (VarAdding == false && VarEditing)
            {
                if (txtName.Text != "" && txtUser.Text != "" && txtPass.Text != "")// && cmbSecond_SU.SelectedIndex != -1 && cmbThird_SU.SelectedIndex != -1)
                {
                    if (txtPass.Text.Length < 4)
                        MessageBox.Show("گذر واژه حداقل باید 4 کاراکتر باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        var nb_typist = proc_strTOint32(txtNb.Text.Trim());
                        var userName = Hashing(txtUser.Text.Trim());
                        var exist = PtcsEntities.Tbl_Typist.Any(a => a.nb != nb_typist && a.username == userName);

                        if (!exist)
                        {

                            var query = PtcsEntities.Tbl_Typist
                                         .Where(i => i.nb == nb_typist)
                                         .Select(i => i)
                                         .FirstOrDefault();
                            if (query != null)
                            {
                                query.active = Active;
                                query.name = txtName.Text;
                                query.username = Hashing(txtUser.Text.Trim());
                                query.password = Hashing(txtPass.Text.Trim());
                                query.date_begin = txtDate_begin.Text;
                                query.date_end = txtDate_end.Text;
                                query.post = txtPost.Text;

                                PtcsEntities.SaveChanges();
                            }



                            VarAdding = false;
                            VarEditing = false;
                            proc_EnDis();

                            proc_refresh();

                        }
                        else
                        {
                            MessageBox.Show("نام کاربری تکراری می باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                }
                else
                {
                    MessageBox.Show("اطلاعات را کامل وارد کنید", "اخطار");
                }
            }
        }

        private string Hashing(string n)
        {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] bytes = ue.GetBytes(n);

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);

            return System.Text.RegularExpressions.Regex.Replace
            (BitConverter.ToString(hashBytes), "-", "").ToLower();
        }

        private void ck_active_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_active.Checked)
                txtPass.UseSystemPasswordChar = true;
            else
                txtPass.UseSystemPasswordChar = false;
        }

        private void frmEdit_typist_KeyDown(object sender, KeyEventArgs e)
        {
            var permission = PtcsEntities.Tbl_Typist
                                        .Where(i => i.nb == CNbTypist)
                                        .Select(i => i.access_inprog)
                                        .FirstOrDefault();



            if ((permission.Contains(",2-1-2,") && (permission.IndexOf(",2-1-2") == 0 || permission.Substring(permission.IndexOf(",2-1-2") - 1, 1) != "-")) || permission.Contains(",2-1-2") || permission.StartsWith("2-1-2"))
            {

                if (e.KeyCode == Keys.F2)
                {
                    if (_fAcces && VarAdding == false && VarEditing == false)
                    {
                        trvAccessInprog.Enabled = true;
                        trvAccessInprog.Focus();
                        _fAcces = false;
                        btnNew.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSearch.Enabled = false;
                        btnExit.Enabled = false;
                    }
                    else if (txtNb.Text != "" && VarAdding == false && VarEditing == false)
                    {
                        trvAccessInprog.Enabled = false;
                        btnNew.Focus();
                        _fAcces = true;
                        btnNew.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSearch.Enabled = true;
                        btnExit.Enabled = true;
                    }

                }
                if (e.KeyCode == Keys.Escape)
                    if (!VarAdding && !VarEditing)
                        Close();

                if (e.KeyCode == Keys.F7)
                    btnList_Click(sender, e);

                if (!VarAdding && !VarEditing)
                {
                    if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.None)
                    {
                        var numberBase = Convert.ToInt32(txtNb.Text);
                        var query = PtcsEntities.Tbl_Typist
                                    .Where(item => item.nb < numberBase)
                                    .OrderByDescending(item => item.nb)
                                    .Select(item => item)
                                    .FirstOrDefault();

                        if (_fAcces)
                        {
                            if (query != null)
                            {
                                proc_trv_empty(trvAccessInprog);
                                proc_viewaccess(query.access_inprog);
                                txtNb.Text = query.nb.ToString();
                                proc_refresh();
                            }

                            else
                                MessageBox.Show("اولین کاربر است");
                        }

                    }

                    if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.None)
                    {

                        var numberBase = Convert.ToInt32(txtNb.Text);
                        var query = PtcsEntities.Tbl_Typist
                                    .Where(item => item.nb > numberBase)
                                    .OrderBy(item => item.nb)
                                    .Select(item => item)
                                    .FirstOrDefault();

                        if (_fAcces)
                        {
                            if (query != null)
                            {
                                proc_trv_empty(trvAccessInprog);
                                proc_viewaccess(query.access_inprog);
                                txtNb.Text = query.nb.ToString();
                                proc_refresh();
                            }
                            else
                                MessageBox.Show("آخرین کاربر است");
                        }
                    }

                    if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.Control)
                    {
                        var query = PtcsEntities.Tbl_Typist
                                     .OrderBy(item => item.nb)
                                     .Select(item => item)
                                     .FirstOrDefault();
                        if (_fAcces)
                        {
                            if (query != null)
                            {
                                proc_trv_empty(trvAccessInprog);
                                proc_viewaccess(query.access_inprog);
                                txtNb.Text = query.nb.ToString();
                                proc_refresh();
                            }
                        }
                    }

                    if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Control)
                    {
                        var query = PtcsEntities.Tbl_Typist
                                    .OrderByDescending(item => item.nb)
                                    .Select(item => item)
                                    .FirstOrDefault();
                        if (_fAcces)
                        {
                            if (query != null)
                            {
                                proc_trv_empty(trvAccessInprog);
                                proc_viewaccess(query.access_inprog);
                                txtNb.Text = query.nb.ToString();
                                proc_refresh();
                            }
                        }
                    }


                }


            }

        }

        private void btnNew_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (rdActive1_1.Checked)
                Active = true;
            else
                Active = false;

            var query = (from i in PtcsEntities.Tbl_Typist
                         where (txtUser2.Text.Trim() != "" ? i.username == (Hashing(txtUser2.Text)) : true) &&
                               (txtName2.Text.Trim() != "" ? i.name == (txtName2.Text) : true) &&
                               i.active == Active
                         select i).FirstOrDefault();

            if (query != null) txtNb.Text = query.nb.ToString();
            {
                proc_refresh();

                txtName2.Text = "";
                txtUser2.Text = "";

                panel2.Visible = false;
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            //if (txtUser.Text != "")
            //    txtUser.Text = hashing(txtUser.Text);
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            //if (txtPass.Text != "")
            //    txtPass.Text = hashing(txtPass.Text);
        }


        private void button6_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
        }

        private void trvAccessInprog_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (_varview && txtNb.Text != "")
            {
                var numberBase = Convert.ToInt32(txtNb.Text.Trim());
                var query = (from a in PtcsEntities.Tbl_Typist
                             where a.nb == numberBase
                             select a).FirstOrDefault();
                if (query != null)
                {
                    if (e.Node.Checked)
                    {
                        query.access_inprog = query.access_inprog.Trim() + e.Node.Name + ",";
                        PtcsEntities.SaveChanges();
                        if (e.Node.Name.Split(new Char[] { '-' }).Length > 1 && !e.Node.Parent.Checked)
                            e.Node.Parent.Checked = true;
                    }
                    else
                    {
                        int s;
                        if (query != null && query.access_inprog.Trim().StartsWith(e.Node.Name + ","))
                            s = 0;
                        else
                            s = query.access_inprog.Trim().IndexOf("," + e.Node.Name + ",");
                        if (s >= 0)
                        {
                            query.access_inprog = query.access_inprog.Trim().Substring(0, s) + query.access_inprog.Trim().Substring(s + e.Node.Name.Trim().Length + 1);
                            PtcsEntities.SaveChanges();
                        }
                        if (e.Node.GetNodeCount(true) > 0)
                            foreach (TreeNode l in e.Node.Nodes)
                                l.Checked = false;
                    }
                }
            }
        }


        private void proc_trv_empty(TreeView tree)
        {
            foreach (TreeNode q in tree.Nodes)
            {
                q.Checked = false;
                foreach (TreeNode w in q.Nodes)
                {
                    w.Checked = false;
                    foreach (TreeNode e in w.Nodes)
                    {
                        e.Checked = false;
                        foreach (TreeNode r in e.Nodes)
                        {
                            r.Checked = false;
                            foreach (TreeNode t in r.Nodes)
                            {
                                t.Checked = false;
                                foreach (TreeNode y in t.Nodes)
                                {
                                    y.Checked = false;
                                    foreach (TreeNode u in y.Nodes)
                                    {
                                        u.Checked = false;
                                        foreach (TreeNode i in u.Nodes)
                                        {
                                            i.Checked = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            var aa = Encoding.ASCII.GetBytes(txtUser.Text.Trim());
            MessageBox.Show(ToHex(aa, false));
        }
        public static string ToHex(byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var query = PtcsEntities.Tbl_Typist
                 .Where(i => i.nb == CNbTypist)
                 .Select(i => i.access_inprog)
                 .FirstOrDefault();

            if (query != null && (query.Contains(",2-1-1,") || query.Contains("2-1-1,")))
            {
                panel2.Visible = false;
                VarEditing = true;
                proc_EnDis();
                RecordActive = txtNb.Text;

            }
            else
                XtraMessageBox.Show("کاربر محترم شما مجوز ویرایش اطلاعات کاربر را ندارید", "آگهی",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void trvAccessInprog_Enter(object sender, EventArgs e)
        {
            _varview = true;
        }

        private void trvAccessInprog_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            

        }

        private void trvAccessInprog_Leave(object sender, EventArgs e)
        {
            _varview = false;
            trvAccessInprog.CollapseAll();
        }


    }
}
