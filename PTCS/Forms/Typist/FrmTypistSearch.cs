﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.Typist
{
    public partial class FrmTypistSearch : XtraForm
    {
        public PTCSEntities PtcsEntities;
        private Tbl_Expert _tblExpert;

        public string Ss;

        public int CNbTypist;
        public FrmTypistSearch(int nbTypist)
        {
            CNbTypist = nbTypist;
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            _tblExpert = new Tbl_Expert();
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            proc_find();
        }

        private void proc_find()
        {
            var query = (from i in PtcsEntities.Tbl_Typist
                         where
                         txtExpertName.Text.Trim() != "" ? i.name.Contains(txtExpertName.Text.Trim()) : true

                         select new
                         {
                             Id = i.nb,
                             Name = i.name,
                             active = i.active ? "فعال" : "غیر فعال"
                         }).ToList();
            if (query.Count > 0)
            {
                dataGridView1.DataSource = query;

                dataGridView1.Columns[0].Visible = false;

                dataGridView1.Columns[1].HeaderText = "نام و نام خانوادگی";
                dataGridView1.Columns[1].Width = 550;

                dataGridView1.Columns[2].HeaderText = "وضعیت";
                dataGridView1.Columns[2].Width = 100;
            }


        }

        private void FrmExpertSearch_Load(object sender, EventArgs e)
        {
            proc_find();
        }

        private void txtExpertName_TextChanged(object sender, EventArgs e)
        {
            proc_find();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView1.DataSource != null)
            {
                Ss = dataGridView1.Rows[(sender as DataGridView).CurrentCell.RowIndex].Cells[0].Value.ToString();

                this.Close();

            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.DataSource != null)
                {
                    Ss = dataGridView1.Rows[(sender as DataGridView).CurrentCell.RowIndex].Cells[0].Value.ToString();

                    this.Close();

                }
            }
        }
    }
}
