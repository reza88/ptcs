﻿using DevExpress.XtraEditors;

namespace PTCS.Forms.Typist
{
    partial class Frmtypist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmtypist));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdActive2 = new System.Windows.Forms.RadioButton();
            this.rdActive1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.trvAccessInprog = new System.Windows.Forms.TreeView();
            this.ck_active = new System.Windows.Forms.CheckBox();
            this.txtNb = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdActive2_2 = new System.Windows.Forms.RadioButton();
            this.rdActive1_1 = new System.Windows.Forms.RadioButton();
            this.label34 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtUser2 = new System.Windows.Forms.MaskedTextBox();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtDate_end = new System.Windows.Forms.MaskedTextBox();
            this.txtDate_begin = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPost = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdActive2);
            this.groupBox1.Controls.Add(this.rdActive1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(438, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(342, 52);
            this.groupBox1.TabIndex = 1059;
            this.groupBox1.TabStop = false;
            // 
            // rdActive2
            // 
            this.rdActive2.AutoSize = true;
            this.rdActive2.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.rdActive2.Location = new System.Drawing.Point(17, 12);
            this.rdActive2.Margin = new System.Windows.Forms.Padding(4);
            this.rdActive2.Name = "rdActive2";
            this.rdActive2.Size = new System.Drawing.Size(91, 34);
            this.rdActive2.TabIndex = 1;
            this.rdActive2.Text = "غیر فعال";
            this.rdActive2.UseVisualStyleBackColor = true;
            // 
            // rdActive1
            // 
            this.rdActive1.AutoSize = true;
            this.rdActive1.Checked = true;
            this.rdActive1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.rdActive1.Location = new System.Drawing.Point(154, 11);
            this.rdActive1.Margin = new System.Windows.Forms.Padding(4);
            this.rdActive1.Name = "rdActive1";
            this.rdActive1.Size = new System.Drawing.Size(63, 34);
            this.rdActive1.TabIndex = 0;
            this.rdActive1.TabStop = true;
            this.rdActive1.Text = "فعال";
            this.rdActive1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(256, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 30);
            this.label3.TabIndex = 0;
            this.label3.Text = "وضعیت :";
            // 
            // trvAccessInprog
            // 
            this.trvAccessInprog.BackColor = System.Drawing.SystemColors.ControlLight;
            this.trvAccessInprog.CheckBoxes = true;
            this.trvAccessInprog.Enabled = false;
            this.trvAccessInprog.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.trvAccessInprog.Location = new System.Drawing.Point(438, 92);
            this.trvAccessInprog.Margin = new System.Windows.Forms.Padding(4);
            this.trvAccessInprog.Name = "trvAccessInprog";
            this.trvAccessInprog.Size = new System.Drawing.Size(341, 404);
            this.trvAccessInprog.TabIndex = 12;
            this.trvAccessInprog.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.trvAccessInprog_AfterCheck);
            this.trvAccessInprog.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trvAccessInprog_AfterSelect);
            this.trvAccessInprog.Enter += new System.EventHandler(this.trvAccessInprog_Enter);
            this.trvAccessInprog.Leave += new System.EventHandler(this.trvAccessInprog_Leave);
            // 
            // ck_active
            // 
            this.ck_active.AutoSize = true;
            this.ck_active.Checked = true;
            this.ck_active.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ck_active.Location = new System.Drawing.Point(8, 108);
            this.ck_active.Margin = new System.Windows.Forms.Padding(4);
            this.ck_active.Name = "ck_active";
            this.ck_active.Size = new System.Drawing.Size(18, 17);
            this.ck_active.TabIndex = 1155;
            this.ck_active.UseVisualStyleBackColor = true;
            this.ck_active.CheckedChanged += new System.EventHandler(this.ck_active_CheckedChanged);
            // 
            // txtNb
            // 
            this.txtNb.Enabled = false;
            this.txtNb.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtNb.Location = new System.Drawing.Point(410, 48);
            this.txtNb.Margin = new System.Windows.Forms.Padding(4);
            this.txtNb.Name = "txtNb";
            this.txtNb.Size = new System.Drawing.Size(29, 29);
            this.txtNb.TabIndex = 1157;
            this.txtNb.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label14.Location = new System.Drawing.Point(589, 62);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 30);
            this.label14.TabIndex = 1158;
            this.label14.Text = "دسترسی";
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(15, 502);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(71, 37);
            this.btnNew.TabIndex = 2;
            this.btnNew.Text = "جدید";
            this.toolTip1.SetToolTip(this.btnNew, "راست کلیک = ویرایش");
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnNew.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnNew_MouseDown);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightGray;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(410, 126);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(29, 50);
            this.panel2.TabIndex = 1159;
            this.panel2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.txtUser2);
            this.groupBox2.Controls.Add(this.txtName2);
            this.groupBox2.Location = new System.Drawing.Point(4, -1);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(606, 154);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdActive2_2);
            this.groupBox4.Controls.Add(this.rdActive1_1);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Location = new System.Drawing.Point(8, 15);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(253, 49);
            this.groupBox4.TabIndex = 1081;
            this.groupBox4.TabStop = false;
            // 
            // rdActive2_2
            // 
            this.rdActive2_2.AutoSize = true;
            this.rdActive2_2.Location = new System.Drawing.Point(18, 18);
            this.rdActive2_2.Margin = new System.Windows.Forms.Padding(4);
            this.rdActive2_2.Name = "rdActive2_2";
            this.rdActive2_2.Size = new System.Drawing.Size(78, 21);
            this.rdActive2_2.TabIndex = 1016;
            this.rdActive2_2.Text = "غیر فعال";
            this.rdActive2_2.UseVisualStyleBackColor = true;
            // 
            // rdActive1_1
            // 
            this.rdActive1_1.AutoSize = true;
            this.rdActive1_1.Checked = true;
            this.rdActive1_1.Location = new System.Drawing.Point(115, 18);
            this.rdActive1_1.Margin = new System.Windows.Forms.Padding(4);
            this.rdActive1_1.Name = "rdActive1_1";
            this.rdActive1_1.Size = new System.Drawing.Size(55, 21);
            this.rdActive1_1.TabIndex = 1015;
            this.rdActive1_1.TabStop = true;
            this.rdActive1_1.Text = "فعال";
            this.rdActive1_1.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label34.Location = new System.Drawing.Point(178, 15);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 22);
            this.label34.TabIndex = 1014;
            this.label34.Text = "وضعیت :";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button6.Location = new System.Drawing.Point(139, 111);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(71, 32);
            this.button6.TabIndex = 4;
            this.button6.Text = "خروج";
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button5.Location = new System.Drawing.Point(375, 113);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(71, 32);
            this.button5.TabIndex = 3;
            this.button5.Text = "جستجو";
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label35.Location = new System.Drawing.Point(476, 31);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(87, 22);
            this.label35.TabIndex = 1079;
            this.label35.Text = "نام کاربری :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label36.Location = new System.Drawing.Point(476, 68);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(135, 22);
            this.label36.TabIndex = 1058;
            this.label36.Text = "نام و نام خانوادگی :";
            // 
            // txtUser2
            // 
            this.txtUser2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser2.Location = new System.Drawing.Point(342, 26);
            this.txtUser2.Margin = new System.Windows.Forms.Padding(4);
            this.txtUser2.Name = "txtUser2";
            this.txtUser2.Size = new System.Drawing.Size(126, 29);
            this.txtUser2.TabIndex = 0;
            this.txtUser2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // txtName2
            // 
            this.txtName2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtName2.Location = new System.Drawing.Point(284, 64);
            this.txtName2.Margin = new System.Windows.Forms.Padding(4);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(183, 29);
            this.txtName2.TabIndex = 1;
            this.txtName2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtDate_end);
            this.groupBox5.Controls.Add(this.txtDate_begin);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.txtPost);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtUser);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.txtPass);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.ck_active);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.txtName);
            this.groupBox5.Location = new System.Drawing.Point(10, 21);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(392, 476);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // txtDate_end
            // 
            this.txtDate_end.Enabled = false;
            this.txtDate_end.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDate_end.Location = new System.Drawing.Point(32, 185);
            this.txtDate_end.Margin = new System.Windows.Forms.Padding(4);
            this.txtDate_end.Mask = "0000/00/00";
            this.txtDate_end.Name = "txtDate_end";
            this.txtDate_end.PromptChar = ' ';
            this.txtDate_end.Size = new System.Drawing.Size(218, 29);
            this.txtDate_end.TabIndex = 4;
            this.txtDate_end.ValidatingType = typeof(System.DateTime);
            this.txtDate_end.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // txtDate_begin
            // 
            this.txtDate_begin.Enabled = false;
            this.txtDate_begin.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDate_begin.Location = new System.Drawing.Point(32, 149);
            this.txtDate_begin.Margin = new System.Windows.Forms.Padding(4);
            this.txtDate_begin.Mask = "0000/00/00";
            this.txtDate_begin.Name = "txtDate_begin";
            this.txtDate_begin.PromptChar = ' ';
            this.txtDate_begin.Size = new System.Drawing.Size(218, 29);
            this.txtDate_begin.TabIndex = 3;
            this.txtDate_begin.ValidatingType = typeof(System.DateTime);
            this.txtDate_begin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(259, 220);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 30);
            this.label7.TabIndex = 1082;
            this.label7.Text = "سمت سازمانی :";
            // 
            // txtPost
            // 
            this.txtPost.Enabled = false;
            this.txtPost.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtPost.Location = new System.Drawing.Point(32, 220);
            this.txtPost.Margin = new System.Windows.Forms.Padding(4);
            this.txtPost.Name = "txtPost";
            this.txtPost.Size = new System.Drawing.Size(218, 29);
            this.txtPost.TabIndex = 5;
            this.txtPost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(259, 184);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 30);
            this.label6.TabIndex = 1081;
            this.label6.Text = "تاریخ اتمام کار :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(259, 149);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 30);
            this.label4.TabIndex = 1080;
            this.label4.Text = "تاریخ شروع به کار :";
            // 
            // txtUser
            // 
            this.txtUser.Enabled = false;
            this.txtUser.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtUser.Location = new System.Drawing.Point(32, 71);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(218, 29);
            this.txtUser.TabIndex = 1;
            this.txtUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            this.txtUser.Leave += new System.EventHandler(this.txtUser_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(259, 107);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 30);
            this.label1.TabIndex = 1079;
            this.label1.Text = "گذرواژه :";
            // 
            // txtPass
            // 
            this.txtPass.Enabled = false;
            this.txtPass.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtPass.Location = new System.Drawing.Point(32, 107);
            this.txtPass.Margin = new System.Windows.Forms.Padding(4);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(218, 29);
            this.txtPass.TabIndex = 2;
            this.txtPass.UseSystemPasswordChar = true;
            this.txtPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            this.txtPass.Leave += new System.EventHandler(this.txtPass_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(259, 33);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 30);
            this.label5.TabIndex = 1078;
            this.label5.Text = "نام و نام خانوادگی :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(259, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 30);
            this.label2.TabIndex = 1077;
            this.label2.Text = "نام کاربری :";
            // 
            // txtName
            // 
            this.txtName.Enabled = false;
            this.txtName.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtName.Location = new System.Drawing.Point(69, 31);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(182, 29);
            this.txtName.TabIndex = 0;
            this.txtName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // btnExit
            // 
            this.btnExit.Appearance.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnExit.Appearance.Options.UseFont = true;
            this.btnExit.Location = new System.Drawing.Point(692, 502);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(71, 37);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "خروج";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Location = new System.Drawing.Point(530, 502);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 37);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "جستجو";
            this.btnSearch.Click += new System.EventHandler(this.btnList_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(393, 502);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 37);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "لغو";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(257, 502);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 37);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "ذخیره";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(131, 502);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(71, 37);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "ویرایش";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            this.btnEdit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnNew_MouseDown);
            // 
            // Frmtypist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 545);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtNb);
            this.Controls.Add(this.trvAccessInprog);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Frmtypist";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "                                                                                 " +
    "        فرم تعریف کاربران";
            this.Load += new System.EventHandler(this.frmEdit_typist_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEdit_typist_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView trvAccessInprog;
        private System.Windows.Forms.CheckBox ck_active;
        private System.Windows.Forms.TextBox txtNb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton rdActive2;
        private System.Windows.Forms.RadioButton rdActive1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.MaskedTextBox txtUser2;
        private System.Windows.Forms.TextBox txtName2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdActive2_2;
        private System.Windows.Forms.RadioButton rdActive1_1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private System.Windows.Forms.MaskedTextBox txtDate_end;
        private System.Windows.Forms.MaskedTextBox txtDate_begin;
        private SimpleButton btnEdit;
    }
}