﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using MathWorks.MATLAB.NET.Arrays;
using PTCS.Methods;
using PTCSModel;
using PTCSModel.Classes;
using PTCSModel.Senarios;

namespace PTCS.Forms.Senario
{
    public partial class FrmSenario : XtraForm
    {
        readonly int _cNbTypist;
        //public string record_active;
        private bool _varAdding;
        private bool _varEditing;
        private int _autoKey;
        private Guid SenarioId { get; set; }
        public readonly PTCSEntities PtcsEntities;
        public FrmSenario(int nbTypist)
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;

            SkinManager.EnableFormSkins();
            _cNbTypist = nbTypist;

            InitializeComponent();
        }

        private void cmbTechnology_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{tab}");
            if (e.KeyCode == Keys.Right)
                SendKeys.Send("+{tab}");
        }

        private void cmbTechnology_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void btnCreateSenario_Click(object sender, EventArgs e)
        {

            txtQue1.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            txtQue2.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            txtQue3.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            txtQue4.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;

            if (!string.IsNullOrEmpty(txtQue1.Text) && txtQue1.Text != "0" &&
                !string.IsNullOrEmpty(txtQue2.Text) && txtQue2.Text != "0" &&
                !string.IsNullOrEmpty(txtQue3.Text) && txtQue3.Text != "0" &&
                !string.IsNullOrEmpty(txtQue4.Text) && txtQue4.Text != "0")
            {
                var permission = PtcsEntities.Tbl_Typist.Where(i => i.nb == _cNbTypist).Select(i => i.access_inprog);
                if (permission.Count() != 0)
                {
                    txtQue1.TextMaskFormat = MaskFormat.IncludeLiterals;
                    txtQue2.TextMaskFormat = MaskFormat.IncludeLiterals;
                    txtQue3.TextMaskFormat = MaskFormat.IncludeLiterals;
                    txtQue4.TextMaskFormat = MaskFormat.IncludeLiterals;
                    var firstOrDefault = permission.FirstOrDefault();
                    if (firstOrDefault != null && (firstOrDefault.Contains(",3-1,") || firstOrDefault.Contains("3-1,")))
                    {
                        string status = "";
                        var tecnologyId = (from i in PtcsEntities.Tbl_Technology
                                           where i.TechnologyName == cmbTechnology.Text.Trim()
                                           select i.TechnologyId).FirstOrDefault();

                        var query = from i in PtcsEntities.Tbl_SenarioDetail
                                    join j in PtcsEntities.Tbl_Senario on i.SenarioId equals j.SenarioId
                                    where j.TechnologyId == tecnologyId
                                    select i;

                        if (query.Count() != 0)
                        {
                            if (query.FirstOrDefault(item => item.IsMain == true) != null)
                            {
                                try
                                {
                                    Cursor = Cursors.WaitCursor;

                                    MWArray x1;
                                    MWArray x2;
                                    MWArray x3;
                                    MWArray x4;
                                    var aa = Convert.ToDouble(txtQue1.Text.Trim());
                                    var bb = Convert.ToDouble(txtQue2.Text.Trim());
                                    var cc = Convert.ToDouble(txtQue3.Text.Trim());
                                    var dd = Convert.ToDouble(txtQue4.Text.Trim());

                                    x1 = aa;
                                    x2 = bb;
                                    x3 = cc;
                                    x4 = dd;

                                    var result = new PPK.Predict().PTCS(x1, x2, x3, x4);

                                    decimal ee;

                                    try
                                    {
                                        ee = Convert.ToDecimal(result.ToString().Replace('.', '/'));
                                    }
                                    catch (Exception exception)
                                    {
                                        ee = Convert.ToDecimal(result.ToString());
                                    }
                                    ee = Math.Round(ee, 2);
                                    //if (appSetting.WindowsType == WindowsType.win7)
                                    //    ee = Convert.ToDecimal(result.ToString());
                                    //else
                                    //    ee = Convert.ToDecimal(Math.Round(Convert.ToDouble(ee), 2));

                                    if (ee > 0 && ee < 20)
                                        status = "Very Low";
                                    if (ee > 20 && ee < 40)
                                        status = "Low";
                                    if (ee > 40 && ee < 60)
                                        status = "Medium";
                                    if (ee > 60 && ee < 80)
                                        status = "High";
                                    if (ee > 80 && ee < 100)
                                        status = "Very High";

                                    PtcsEntities.Tbl_SenarioDetail.Add(new Tbl_SenarioDetail
                                    {
                                        SenarioDetailId = Guid.NewGuid(),
                                        SenarioId = SenarioId,
                                        TechnicalSpecifications = Convert.ToDecimal(aa),
                                        FinancialAndEconomicData = Convert.ToDecimal(bb),
                                        MarketSpecifications = Convert.ToDecimal(cc),
                                        RulesAndApprovals = Convert.ToDecimal(dd),
                                        Result = ee,
                                        Status = status,
                                        IsMain = false
                                    });

                                    PtcsEntities.SaveChanges();
                                    GetAllSenarioDetail();

                                    Cursor = Cursors.Default;
                                }
                                catch (Exception ex)
                                {
                                    Cursor = Cursors.Default;
                                    MessageBox.Show(ex.Message);
                                    //XtraMessageBox.Show("اشکال در محاسبه سناریو", "خطا", MessageBoxButtons.OK,
                                    //    MessageBoxIcon.Error);
                                }


                            }
                        }
                    }
                    else
                        XtraMessageBox.Show("کاربر محترم شما مجوز ایجاد سناریو را ندارید", "آگهی",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void FrmSenario_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo language = new System.Globalization.CultureInfo("fa-IR");
            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(language);

            proc_EnDis();

            var tecnology = PtcsEntities.Tbl_Technology.Select(a => a.TechnologyName).ToList();
            foreach (var rec in tecnology)
            {
                cmbTechnology.Properties.Items.Add(rec);
            }
            cmbTechnology.SelectedIndex = 0;

            ViewSenario vw = new ViewSenario();

            SenarioId = vw.GetLast();

            if (SenarioId != null)
                proc_refresh();

            if (SenarioId == null)
            {
                txtQue1.Enabled = false;
                txtQue2.Enabled = false;
                txtQue4.Enabled = false;
                txtQue3.Enabled = false;
                btnCreateSenario.Enabled = false;
            }
            else
            {
                txtQue1.Enabled = true;
                txtQue2.Enabled = true;
                txtQue4.Enabled = true;
                txtQue3.Enabled = true;
                btnCreateSenario.Enabled = true;
            }


            GetAllSenarioDetail();

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            _varAdding = true;

            proc_empty();

            proc_EnDis();
            cmbTechnology.Focus();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {

            var query = PtcsEntities.Tbl_Typist.Where(i => i.nb == _cNbTypist).Select(i => i.access_inprog);

            if (query.Count() != 0)
            {
                var firstOrDefault = query.FirstOrDefault();
                if (firstOrDefault != null && (firstOrDefault.Contains(",3-1,") || firstOrDefault.Contains("3-1,")))
                {
                    _varEditing = true;
                    proc_EnDis();
                    cmbTechnology.Focus();
                    //record_active = txtNb.Text;
                }
                else
                    XtraMessageBox.Show("کاربر محترم شما مجوز ویرایش اطلاعات خروجی برنامه را ندارید", "آگهی",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbTechnology.Text != string.Empty)
            {

                var technologyId = (from i in PtcsEntities.Tbl_Technology
                                    where i.TechnologyName == cmbTechnology.Text
                                    select i.TechnologyId).FirstOrDefault();

                if (_varAdding && _varEditing == false)
                {
                    bool duplicateExists = PtcsEntities.Tbl_Senario.Any(x => x.TechnologyId == technologyId);
                    if (!duplicateExists)
                    {

                        try
                        {
                            var finalResult = proc_FinalResultCalculate(technologyId);

                            if (!string.IsNullOrEmpty(finalResult.Status))
                            {
                                var result = PtcsEntities.Tbl_Senario.Add(new Tbl_Senario
                                {
                                    SenarioId = Guid.NewGuid(),
                                    TechnologyId = technologyId
                                });

                                PtcsEntities.SaveChanges();
                                if (result != null && result.SenarioId != Guid.Empty)
                                {
                                    SenarioId = result.SenarioId;
                                    proc_refresh();

                                    PtcsEntities.Tbl_SenarioDetail.Add(new Tbl_SenarioDetail
                                    {
                                        SenarioDetailId = Guid.NewGuid(),
                                        SenarioId = SenarioId,
                                        TechnicalSpecifications = Convert.ToDecimal(finalResult.TechnicalSpecifications),
                                        FinancialAndEconomicData = Convert.ToDecimal(finalResult.FinancialAndEconomicData),
                                        MarketSpecifications = Convert.ToDecimal(finalResult.MarketSpecifications),
                                        RulesAndApprovals = Convert.ToDecimal(finalResult.RulesAndApprovals),
                                        Result = finalResult.Result,
                                        Status = finalResult.Status,
                                        IsMain = true
                                    });

                                    PtcsEntities.SaveChanges();
                                }

                            }

                            else
                            {

                                XtraMessageBox.Show("خطا در ثبت خروجی برنامه", "خطا", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                            }
                        }
                        catch (Exception exception)
                        {
                            XtraMessageBox.Show("خطا در ثبت خروجی برنامه", "خطا", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        }



                    }
                    else
                        XtraMessageBox.Show("خروجی برنامه با این فن آوری تکراری می باشد", "خطا", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    _varAdding = false;
                    _varEditing = false;
                    proc_EnDis();

                    GetAllSenarioDetail();

                }


                else if (_varAdding == false && _varEditing)
                {

                    bool duplicateExists = PtcsEntities.Tbl_Senario.Any(x => x.SenarioId != SenarioId && x.TechnologyId == technologyId);
                    if (!duplicateExists)
                    {
                        var updateQuery = from i in PtcsEntities.Tbl_Senario
                                          where i.TechnologyId == technologyId
                                          select i;
                        if (updateQuery.Count() != 0)
                        {
                            var rec = updateQuery.FirstOrDefault();
                            if (rec != null)
                            {
                                rec.TechnologyId = technologyId;
                            }

                            PtcsEntities.SaveChanges();

                            _varAdding = false;
                            _varEditing = false;
                            proc_EnDis();
                            proc_refresh();
                        }
                    }

                    else
                        XtraMessageBox.Show(" خروجی برنامه برای این فن آوری تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                }
            }
            else
                XtraMessageBox.Show("اطلاعات را کامل وارد کنید", "اخطار");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _varAdding = false;
            _varEditing = false;
            proc_EnDis();
            proc_refresh();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void proc_empty()
        {
            cmbTechnology.SelectedIndex = -1;

            cmbTechnology.Focus();
        }

        private void FrmSenario_KeyDown(object sender, KeyEventArgs e)
        {
            ViewSenario vw = new ViewSenario();



            if (!_varAdding && !_varEditing)
            {
                #region previous

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.None)
                {

                    SenarioId = vw.GetPrevious(_autoKey);

                    if (SenarioId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("اولین پرونده است");
                }

                #endregion

                #region Next

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.None)
                {

                    SenarioId = vw.GetNext(_autoKey);

                    if (SenarioId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("آخرین پرونده است");

                }

                #endregion

                #region First

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.Control)
                {
                    SenarioId = vw.GetFirst();

                    if (SenarioId != null)
                        proc_refresh();
                }

                #endregion

                #region Last

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Control)
                {
                    SenarioId = vw.GetLast();

                    if (SenarioId != null)
                        proc_refresh();

                }

                #endregion




            }
        }

        public void proc_EnDis()
        {
            if (_varAdding)
            {
                cmbTechnology.Enabled = true;

                btnNew.Enabled = false;
                btnExit.Enabled = false;
                btnPrint.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;

            }
            else if (_varEditing)
            {
                cmbTechnology.Enabled = true;

                btnNew.Enabled = false;
                btnExit.Enabled = false;
                btnPrint.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;
            }
            else
            {
                cmbTechnology.Enabled = false;

                btnNew.Enabled = true;
                btnExit.Enabled = true;
                btnPrint.Enabled = true;
                btnSave.Enabled = false;
                btnCancel.Enabled = false;
            }
        }

        public void proc_refresh()
        {
            {
                var query = from a in PtcsEntities.Tbl_Senario
                            join b in PtcsEntities.Tbl_Technology on a.TechnologyId equals b.TechnologyId
                            where a.SenarioId == (SenarioId)
                            select new { a, b };
                if (query.Count() != 0)
                {
                    var rec = query.FirstOrDefault();
                    if (rec != null)
                    {
                        _autoKey = rec.a.AutoKey;
                        SenarioId = rec.a.SenarioId;

                        cmbTechnology.Text = rec.b.TechnologyName;
                    }

                    //ProcGetAllSenarioDetail();

                    GetAllSenarioDetail();

                }
            }
        }

        private AllSenarioDetail proc_FinalResultCalculate(Guid? tecnologyId)
        {
            double componentRating;
            byte componentRatingOpinion;
            double dimensionScore = 0;

            //حد نصاب
            var quorum = PtcsEntities.Tbl_Technology
                .Where(a => a.TechnologyName == cmbTechnology.Text.Trim())
                .Select(a => a.LowThreshold)
                .FirstOrDefault();

            AllSenarioDetail allSenarioDetail = new AllSenarioDetail();

            allSenarioDetail.TechnologyId = tecnologyId;
            allSenarioDetail.IsMain = true;

            var questionnaireDimension = (from i in PtcsEntities.Tbl_QuestionnaireDimension
                                          select i)
                                         .OrderBy(item => item.AutoKey)
                                         .ToList();

            if (questionnaireDimension.Count() > 0)
            {
                foreach (var rec1 in questionnaireDimension)
                {
                    dimensionScore = 0;
                    var components = (from i in PtcsEntities.Tbl_Component
                                      where i.TechnologyId == tecnologyId &&
                                            i.QuestionnaireDimensionId == rec1.QuestionnaireDimensionId
                                      select i)
                                     .OrderBy(item => item.AutoKey)
                                     .ToList();
                    if (components.Count() != 0)
                    {
                        foreach (var rec2 in components)
                        {
                            byte componentRatingOpinionCount = 0;
                            var opinionDetail = from i in PtcsEntities.Tbl_ExpertOpinionDetail
                                                where i.ComponentId == rec2.ComponentId
                                                select i.Opinion;

                            if (opinionDetail.Count() > 0)
                            {
                                componentRatingOpinion = 0;
                                foreach (var rec3 in opinionDetail)
                                {
                                    componentRatingOpinion += rec3;
                                    componentRatingOpinionCount++;
                                }
                                componentRating = (double)componentRatingOpinion / componentRatingOpinionCount;
                                componentRating = Math.Round(componentRating, 2);

                            }
                            else
                            {
                                componentRating = 0;
                            }
                            if (rec2.IsDominant)
                            {
                                if (componentRating < Convert.ToDouble(quorum))
                                {
                                    allSenarioDetail = new AllSenarioDetail
                                    {
                                        Status = "Very Low"
                                    };
                                    return allSenarioDetail;
                                }
                            }
                            var aa = componentRating * (double)rec2.ComponentWeight;
                            componentRating = Math.Round(aa, 2);

                            dimensionScore += componentRating;
                            dimensionScore = Math.Round(dimensionScore, 2);
                        }
                    }

                    var acacac = 1;

                    if (rec1.QuestionnaireDimensionId ==
                        QuestionnaireDimensions.TechnicalSpecifications.ApiGetEnumGuid())
                        allSenarioDetail.TechnicalSpecifications = dimensionScore;

                    else if (rec1.QuestionnaireDimensionId ==
                        QuestionnaireDimensions.FinancialAndEconomicData.ApiGetEnumGuid())
                        allSenarioDetail.FinancialAndEconomicData = dimensionScore;

                    else if (rec1.QuestionnaireDimensionId ==
                        QuestionnaireDimensions.MarketSpecifications.ApiGetEnumGuid())
                        allSenarioDetail.MarketSpecifications = dimensionScore;

                    else if (rec1.QuestionnaireDimensionId ==
                        QuestionnaireDimensions.RulesAndApprovals.ApiGetEnumGuid())
                        allSenarioDetail.RulesAndApprovals = dimensionScore;

                }

                var aa1 = 1;

                MWArray result;
                MWArray x1 = allSenarioDetail.TechnicalSpecifications;
                MWArray x2 = allSenarioDetail.FinancialAndEconomicData;
                MWArray x3 = allSenarioDetail.MarketSpecifications;
                MWArray x4 = allSenarioDetail.RulesAndApprovals;
                result = new PPK.Predict().PTCS(x1, x2, x3, x4);
                //var aa = result.ToString();
                //allSenarioDetail.Result = Convert.ToDouble(aa);

                //allSenarioDetail.Result = Convert.ToDecimal(result.ToString().Replace('.', '/'));
                try
                {
                    allSenarioDetail.Result = Convert.ToDecimal(result.ToString().Replace('.', '/'));
                }
                catch (Exception e)
                {
                    allSenarioDetail.Result = Convert.ToDecimal(result.ToString());
                }
                //if (appSetting.WindowsType == WindowsType.win7)

                //else



                allSenarioDetail.Result = Convert.ToDecimal(Math.Round(Convert.ToDouble(allSenarioDetail.Result), 2));


                if (allSenarioDetail.Result > 0 && allSenarioDetail.Result < 20)
                    allSenarioDetail.Status = "Very Low";
                if (allSenarioDetail.Result > 20 && allSenarioDetail.Result < 40)
                    allSenarioDetail.Status = "Low";
                if (allSenarioDetail.Result > 40 && allSenarioDetail.Result < 60)
                    allSenarioDetail.Status = "Medium";
                if (allSenarioDetail.Result > 60 && allSenarioDetail.Result < 80)
                    allSenarioDetail.Status = "High";
                if (allSenarioDetail.Result > 80 && allSenarioDetail.Result < 100)
                    allSenarioDetail.Status = "Very High";
            }

            return allSenarioDetail;
        }

        private void GetAllSenarioDetail()
        {
            List<AllSenarioDetail> allSenarioDetails = new List<AllSenarioDetail>();
            var query = (from i in PtcsEntities.Tbl_SenarioDetail
                         join j in PtcsEntities.Tbl_Senario on i.SenarioId equals j.SenarioId
                         where i.SenarioId == SenarioId
                         select i)
                         .OrderBy(item => item.AutoKey)
                         .ToList();
            if (query.Count() != 0)
            {
                int row = 1;
                for (int i = 0; i < query.Count(); i++)
                {

                    var rec = query[i];
                    if (rec.IsMain == true)
                    {
                        allSenarioDetails.Add(new AllSenarioDetail
                        {
                            SenarioId = rec.SenarioId,
                            SenarioName = "وضعیت اصلی",
                            TechnicalSpecifications = Convert.ToDouble(rec.TechnicalSpecifications),
                            FinancialAndEconomicData = Convert.ToDouble(rec.FinancialAndEconomicData),
                            MarketSpecifications = Convert.ToDouble(rec.MarketSpecifications),
                            RulesAndApprovals = Convert.ToDouble(rec.RulesAndApprovals),
                            IsMain = rec.IsMain,
                            Result = rec.Result,
                            ResultString = rec.Result + " % ",
                            Status = rec.Status,
                            SenarioDetailId = rec.SenarioDetailId
                        });
                    }
                    else
                    {
                        allSenarioDetails.Add(new AllSenarioDetail
                        {
                            SenarioId = rec.SenarioId,
                            SenarioName = "سناریو " + row++,
                            TechnicalSpecifications = Convert.ToDouble(rec.TechnicalSpecifications),
                            FinancialAndEconomicData = Convert.ToDouble(rec.FinancialAndEconomicData),
                            MarketSpecifications = Convert.ToDouble(rec.MarketSpecifications),
                            RulesAndApprovals = Convert.ToDouble(rec.RulesAndApprovals),
                            IsMain = rec.IsMain,
                            Result = rec.Result,
                            ResultString = rec.Result.ToString().PadLeft(1, '0') + " % ",
                            Status = rec.Status,
                            SenarioDetailId = rec.SenarioDetailId
                        });
                    }
                }


                //dataGridView1.DataSource = allSenarioDetails;
                dataGridView1.Rows.Clear();
                for (int i = 0; i < allSenarioDetails.Count(); i++)
                {
                    int n = dataGridView1.Rows.Add();

                    dataGridView1.Rows[n].Cells[0].Value = allSenarioDetails[i].SenarioId;
                    dataGridView1.Rows[n].Cells[1].Value = allSenarioDetails[i].TechnologyId;
                    dataGridView1.Rows[n].Cells[2].Value = allSenarioDetails[i].SenarioName;
                    dataGridView1.Rows[n].Cells[3].Value = allSenarioDetails[i].TechnicalSpecifications;
                    dataGridView1.Rows[n].Cells[4].Value = allSenarioDetails[i].FinancialAndEconomicData;
                    dataGridView1.Rows[n].Cells[5].Value = allSenarioDetails[i].MarketSpecifications;
                    dataGridView1.Rows[n].Cells[6].Value = allSenarioDetails[i].RulesAndApprovals;
                    dataGridView1.Rows[n].Cells[7].Value = allSenarioDetails[i].IsMain;
                    dataGridView1.Rows[n].Cells[8].Value = allSenarioDetails[i].Result;
                    dataGridView1.Rows[n].Cells[9].Value = allSenarioDetails[i].ResultString;
                    dataGridView1.Rows[n].Cells[10].Value = allSenarioDetails[i].Status;
                    dataGridView1.Rows[n].Cells[11].Value = allSenarioDetails[i].SenarioDetailId;
                }

                //dataGridView1.Columns[0].Visible = false;
                //dataGridView1.Columns[1].Visible = false;
                //dataGridView1.Columns[10].Visible = false;
                //dataGridView1.Columns[11].Visible = false;
                //dataGridView1.Columns[7].Visible = false;

                //dataGridView1.Columns[2].Width = 100;
                //dataGridView1.Columns[3].Width = 120;
                //dataGridView1.Columns[4].Width = 180;
                //dataGridView1.Columns[5].Width = 150;
                //dataGridView1.Columns[6].Width = 150;
                //dataGridView1.Columns[8].Width = 110;
                //dataGridView1.Columns[9].Width = 200;

                //dataGridView1.Columns[2].HeaderText = "نام سناریو";
                //dataGridView1.Columns[3].HeaderText = "مشخصات فنی";
                //dataGridView1.Columns[4].HeaderText = "مشخصات مالی و اقتصادی";
                //dataGridView1.Columns[5].HeaderText = "مشخصات بازار";
                //dataGridView1.Columns[6].HeaderText = "قوانین و تائیدیه ها";
                //dataGridView1.Columns[8].HeaderText = "نتیجه نهایی";
                //dataGridView1.Columns[9].HeaderText = "وضعیت موقعیت تجاری سازی";
                //dataGridView1.AllowUserToDeleteRows = true;




                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {

                    if ((bool)dataGridView1.Rows[i].Cells[7].Value)
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.LightGray;
                    }

                    var status = dataGridView1.Rows[i].Cells[10].Value.ToString();

                    switch (status)
                    {
                        case "Very Low":
                            {
                                dataGridView1.Rows[i].Cells[10].Style.ForeColor = Color.Red;
                                dataGridView1.Rows[i].Cells[10].Style.Font = new Font("Times New Roman", 15F, FontStyle.Bold);
                                break;
                            }
                        case "Low":
                            {
                                dataGridView1.Rows[i].Cells[10].Style.ForeColor = Color.Orange;
                                dataGridView1.Rows[i].Cells[10].Style.Font = new Font("Times New Roman", 15F, FontStyle.Bold);
                                break;
                            }
                        case "Medium":
                            {
                                dataGridView1.Rows[i].Cells[10].Style.ForeColor = Color.Goldenrod;
                                dataGridView1.Rows[i].Cells[10].Style.Font = new Font("Times New Roman", 15F, FontStyle.Bold);
                                break;
                            }
                        case "High":
                            {
                                dataGridView1.Rows[i].Cells[10].Style.ForeColor = Color.LawnGreen;
                                dataGridView1.Rows[i].Cells[10].Style.Font = new Font("Times New Roman", 15F, FontStyle.Bold);
                                break;
                            }
                        case "Very High":
                            {
                                dataGridView1.Rows[i].Cells[10].Style.ForeColor = Color.Green;
                                dataGridView1.Rows[i].Cells[10].Style.Font = new Font("Times New Roman", 15F, FontStyle.Bold);
                                break;
                            }



                    }

                }
            }
            if (dataGridView1.Rows.Count > 0)
            {
                groupControl4.Enabled = true;
            }
            else
            {
                groupControl4.Enabled = false;
            }
        }

        private void txtQue1_Leave(object sender, EventArgs e)
        {
            //txtQue1.TextMaskFormat=MaskFormat.IncludeLiterals;
            //if (Convert.ToDecimal(txtQue1.Text) <= 5)
            //{
            //    txtQue1.Text = "0";
            //}

            txtQue1.TextMaskFormat = MaskFormat.IncludeLiterals;
            if (txtQue1.Text.Trim() != "/" && txtQue1.Text.Trim() != ".")
                if (!string.IsNullOrEmpty(txtQue1.Text) && Convert.ToDecimal(txtQue1.Text.PadLeft(0)) > 5)
                {
                    txtQue1.Text = "0";
                }
        }

        private void txtQue2_Leave(object sender, EventArgs e)
        {
            txtQue2.TextMaskFormat = MaskFormat.IncludeLiterals;
            if (txtQue2.Text.Trim() != "/" && txtQue2.Text.Trim() != ".")
                if (!string.IsNullOrEmpty(txtQue2.Text) && Convert.ToDecimal(txtQue2.Text.PadLeft(0)) > 5)
                {
                    txtQue2.Text = "0";
                }
        }

        private void txtQue3_Leave(object sender, EventArgs e)
        {
            txtQue3.TextMaskFormat = MaskFormat.IncludeLiterals;
            if (txtQue3.Text.Trim() != "/" && txtQue3.Text.Trim() != ".")
                if (!string.IsNullOrEmpty(txtQue3.Text) && Convert.ToDecimal(txtQue3.Text.PadLeft(0)) > 5)
                {
                    txtQue3.Text = "0";
                }
        }

        private void txtQue4_Leave(object sender, EventArgs e)
        {
            txtQue4.TextMaskFormat = MaskFormat.IncludeLiterals;
            if (txtQue4.Text.Trim() != "/" && txtQue4.Text.Trim() != ".")
                if (!string.IsNullOrEmpty(txtQue4.Text) && Convert.ToDecimal(txtQue4.Text.PadLeft(0)) > 5)
                {
                    txtQue4.Text = "0";
                }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            List<AllSenarioDetail> allSenarioDetails = new List<AllSenarioDetail>();
            var query = (from i in PtcsEntities.Tbl_SenarioDetail
                         join j in PtcsEntities.Tbl_Senario on i.SenarioId equals j.SenarioId
                         where i.SenarioId == SenarioId
                         select i)
                         .OrderBy(item => item.AutoKey)
                         .ToList();
            if (query.Count() != 0)
            {
                int row = 1;
                for (int i = 0; i < query.Count(); i++)
                {

                    var rec = query[i];
                    if (rec.IsMain == true)
                    {
                        allSenarioDetails.Add(new AllSenarioDetail
                        {
                            SenarioId = rec.SenarioId,
                            SenarioName = "وضعیت اصلی",
                            TechnicalSpecifications = Convert.ToDouble(rec.TechnicalSpecifications),
                            FinancialAndEconomicData = Convert.ToDouble(rec.FinancialAndEconomicData),
                            MarketSpecifications = Convert.ToDouble(rec.MarketSpecifications),
                            RulesAndApprovals = Convert.ToDouble(rec.RulesAndApprovals),
                            IsMain = rec.IsMain,
                            Result = rec.Result,
                            ResultString = rec.Result + " % ",
                            Status = rec.Status
                        });
                    }
                    else
                    {
                        allSenarioDetails.Add(new AllSenarioDetail
                        {
                            SenarioId = rec.SenarioId,
                            SenarioName = "سناریو " + row++,
                            TechnicalSpecifications = Convert.ToDouble(rec.TechnicalSpecifications),
                            FinancialAndEconomicData = Convert.ToDouble(rec.FinancialAndEconomicData),
                            MarketSpecifications = Convert.ToDouble(rec.MarketSpecifications),
                            RulesAndApprovals = Convert.ToDouble(rec.RulesAndApprovals),
                            IsMain = rec.IsMain,
                            Result = rec.Result,
                            ResultString = rec.Result.ToString().PadLeft(1, '0') + " % ",
                            Status = rec.Status
                        });
                    }
                }

                var titr1 = PtcsEntities.Tbl_Parameters.Where(a => a.name_variable == "titr1")
                            .Select(a => a.data_variable)
                            .FirstOrDefault();

                var titr2 = PtcsEntities.Tbl_Parameters.Where(a => a.name_variable == "titr2")
                            .Select(a => a.data_variable)
                            .FirstOrDefault();

                DataSet1 ds = new DataSet1();
                foreach (var rec in allSenarioDetails)
                {
                    ds.DataTable1.AddDataTable1Row(rec.SenarioName, rec.TechnicalSpecifications.ToString(),
                        rec.FinancialAndEconomicData.ToString(),
                        rec.RulesAndApprovals.ToString(), rec.MarketSpecifications.ToString(), rec.Result.ToString(), rec.ResultString, rec.Status);
                }


                if (ds.DataTable1 != null)
                {
                    //stiReport1.ClearAllStates();

                    stiReport1.Dictionary.DataStore.Clear();
                    stiReport1.Dictionary.Databases.Clear();
                    stiReport1.Dictionary.DataSources.Clear();
                    stiReport1.BusinessObjectsStore.Clear();

                    stiReport1.RegData(ds);
                    stiReport1.Dictionary.Synchronize();

                    stiReport1.Compile();

                    //stiReport1.RegData(ds);

                    stiReport1["Titr1"] = titr1;
                    stiReport1["Titr2"] = titr2;
                    stiReport1["TechnologyName"] = cmbTechnology.Text.Trim();
                    stiReport1["date_print"] = proc_datetime_persian(DateTime.Now).Substring(0, 10);


                    stiReport1.Render();
                    stiReport1.Show(false);
                }
            }
        }

        public string proc_datetime_persian(DateTime datetime_miladi)
        {
            System.Globalization.PersianCalendar pc = new System.Globalization.PersianCalendar();
            string dt_per = pc.GetYear(datetime_miladi).ToString().Substring(0, 4) + "/" +
                pc.GetMonth(datetime_miladi).ToString().PadLeft(2, '0') + "/" +
                pc.GetDayOfMonth(datetime_miladi).ToString().PadLeft(2, '0') + " " +
                pc.GetHour(datetime_miladi).ToString().PadLeft(2, '0') + ":" +
                pc.GetMinute(datetime_miladi).ToString().PadLeft(2, '0') + ":" +
                pc.GetSecond(datetime_miladi).ToString().PadLeft(2, '0');
            return dt_per;
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (dataGridView1.CurrentRow != null && dataGridView1.CurrentRow.Cells[2].Value != "وضعیت اصلی")
            {
                if (MessageBox.Show("آیا از حذف ردیف سناریو اطمینان دارید؟", "سوال", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    var senarioDetailId = Guid.Parse(dataGridView1.CurrentRow.Cells[11].Value.ToString());
                    var query = PtcsEntities.Tbl_SenarioDetail
                        .Where(item => item.SenarioDetailId == senarioDetailId)
                        .FirstOrDefault();

                    if (query != null)
                    {
                        PtcsEntities.Tbl_SenarioDetail.Remove(query);
                        PtcsEntities.SaveChanges();
                       
                        
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            GetAllSenarioDetail();
        }
    }
}
