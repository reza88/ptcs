﻿namespace PTCS.Forms.Senario
{
    partial class FrmSenario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSenario));
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cmbTechnology = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.txtQue4 = new System.Windows.Forms.MaskedTextBox();
            this.txtQue3 = new System.Windows.Forms.MaskedTextBox();
            this.txtQue2 = new System.Windows.Forms.MaskedTextBox();
            this.txtQue1 = new System.Windows.Forms.MaskedTextBox();
            this.btnCreateSenario = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.stiReport1 = new Stimulsoft.Report.StiReport();
            this.dataSet11 = new PTCS.Forms.Senario.DataSet1();
            this.stiReportDataSource1 = new Stimulsoft.Report.Design.StiReportDataSource("DataSet1", this.dataSet11);
            this.SenId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TechId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SenName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TechnicalSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinancialAndEco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RulesAndApprovals = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Res = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultStr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SenDetId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTechnology.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.btnPrint);
            this.groupControl3.Controls.Add(this.btnExit);
            this.groupControl3.Controls.Add(this.btnNew);
            this.groupControl3.Controls.Add(this.btnCancel);
            this.groupControl3.Controls.Add(this.btnSave);
            this.groupControl3.Location = new System.Drawing.Point(6, 123);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(1187, 65);
            this.groupControl3.TabIndex = 2;
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(422, 7);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(115, 48);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "چاپ";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.Appearance.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.btnExit.Appearance.Options.UseFont = true;
            this.btnExit.Location = new System.Drawing.Point(301, 8);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(115, 48);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "خروج";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(813, 7);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(118, 48);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "جدید";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(543, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 48);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "لغو";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(664, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(143, 48);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "ذخیره";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.dataGridView1);
            this.groupControl2.Location = new System.Drawing.Point(6, 398);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1187, 349);
            this.groupControl2.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SenId,
            this.TechId,
            this.SenName,
            this.TechnicalSpec,
            this.FinancialAndEco,
            this.MarketSpec,
            this.RulesAndApprovals,
            this.IsM,
            this.Res,
            this.ResultStr,
            this.Status,
            this.SenDetId});
            this.dataGridView1.Location = new System.Drawing.Point(9, 29);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1170, 314);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView1_UserDeletedRow);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cmbTechnology);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Location = new System.Drawing.Point(6, 8);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1187, 109);
            this.groupControl1.TabIndex = 1;
            // 
            // cmbTechnology
            // 
            this.cmbTechnology.Location = new System.Drawing.Point(721, 45);
            this.cmbTechnology.Name = "cmbTechnology";
            this.cmbTechnology.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12.8F);
            this.cmbTechnology.Properties.Appearance.Options.UseFont = true;
            this.cmbTechnology.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTechnology.Size = new System.Drawing.Size(351, 32);
            this.cmbTechnology.TabIndex = 0;
            this.cmbTechnology.SelectedIndexChanged += new System.EventHandler(this.cmbTechnology_SelectedIndexChanged);
            this.cmbTechnology.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTechnology_KeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.8F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(1078, 50);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(93, 21);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "نام فن آوری :";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.txtQue4);
            this.groupControl4.Controls.Add(this.txtQue3);
            this.groupControl4.Controls.Add(this.txtQue2);
            this.groupControl4.Controls.Add(this.txtQue1);
            this.groupControl4.Controls.Add(this.btnCreateSenario);
            this.groupControl4.Controls.Add(this.labelControl5);
            this.groupControl4.Controls.Add(this.labelControl4);
            this.groupControl4.Controls.Add(this.labelControl3);
            this.groupControl4.Controls.Add(this.labelControl1);
            this.groupControl4.Location = new System.Drawing.Point(6, 194);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1187, 196);
            this.groupControl4.TabIndex = 3;
            this.groupControl4.Text = "سناریو جدید";
            // 
            // txtQue4
            // 
            this.txtQue4.Font = new System.Drawing.Font("Tahoma", 13.8F);
            this.txtQue4.Location = new System.Drawing.Point(795, 153);
            this.txtQue4.Mask = "0.00";
            this.txtQue4.Name = "txtQue4";
            this.txtQue4.PromptChar = ' ';
            this.txtQue4.Size = new System.Drawing.Size(156, 35);
            this.txtQue4.TabIndex = 3;
            this.txtQue4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTechnology_KeyDown);
            this.txtQue4.Leave += new System.EventHandler(this.txtQue4_Leave);
            // 
            // txtQue3
            // 
            this.txtQue3.Font = new System.Drawing.Font("Tahoma", 13.8F);
            this.txtQue3.Location = new System.Drawing.Point(795, 112);
            this.txtQue3.Mask = "0.00";
            this.txtQue3.Name = "txtQue3";
            this.txtQue3.PromptChar = ' ';
            this.txtQue3.Size = new System.Drawing.Size(156, 35);
            this.txtQue3.TabIndex = 2;
            this.txtQue3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTechnology_KeyDown);
            this.txtQue3.Leave += new System.EventHandler(this.txtQue3_Leave);
            // 
            // txtQue2
            // 
            this.txtQue2.Font = new System.Drawing.Font("Tahoma", 13.8F);
            this.txtQue2.Location = new System.Drawing.Point(795, 71);
            this.txtQue2.Mask = "0.00";
            this.txtQue2.Name = "txtQue2";
            this.txtQue2.PromptChar = ' ';
            this.txtQue2.Size = new System.Drawing.Size(156, 35);
            this.txtQue2.TabIndex = 1;
            this.txtQue2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTechnology_KeyDown);
            this.txtQue2.Leave += new System.EventHandler(this.txtQue2_Leave);
            // 
            // txtQue1
            // 
            this.txtQue1.Font = new System.Drawing.Font("Tahoma", 13.8F);
            this.txtQue1.Location = new System.Drawing.Point(795, 30);
            this.txtQue1.Mask = "0.00";
            this.txtQue1.Name = "txtQue1";
            this.txtQue1.PromptChar = ' ';
            this.txtQue1.Size = new System.Drawing.Size(156, 35);
            this.txtQue1.TabIndex = 0;
            this.txtQue1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTechnology_KeyDown);
            this.txtQue1.Leave += new System.EventHandler(this.txtQue1_Leave);
            // 
            // btnCreateSenario
            // 
            this.btnCreateSenario.Appearance.Font = new System.Drawing.Font("Tahoma", 10.8F);
            this.btnCreateSenario.Appearance.Options.UseFont = true;
            this.btnCreateSenario.Location = new System.Drawing.Point(523, 32);
            this.btnCreateSenario.Name = "btnCreateSenario";
            this.btnCreateSenario.Size = new System.Drawing.Size(208, 48);
            this.btnCreateSenario.TabIndex = 4;
            this.btnCreateSenario.Text = "نتیجه سناریو";
            this.btnCreateSenario.Click += new System.EventHandler(this.btnCreateSenario_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.8F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(957, 119);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(113, 21);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "مشخصات بازار :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.8F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(957, 158);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(137, 21);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "قوانین و تائیدیه ها :";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.8F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(957, 76);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(203, 21);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = " مشخصات مالی و اقتصادی :";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.8F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(957, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(117, 21);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "مشخصات فنی :";
            // 
            // stiReport1
            // 
            this.stiReport1.EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV2;
            this.stiReport1.ReferencedAssemblies = new string[] {
        "System.Dll",
        "System.Drawing.Dll",
        "System.Windows.Forms.Dll",
        "System.Data.Dll",
        "System.Xml.Dll",
        "Stimulsoft.Controls.Dll",
        "Stimulsoft.Base.Dll",
        "Stimulsoft.Report.Dll"};
            this.stiReport1.ReportAlias = "Report";
            this.stiReport1.ReportDataSources.Add(this.stiReportDataSource1);
            this.stiReport1.ReportGuid = "e675ae7107914f3789429d6934ab2618";
            this.stiReport1.ReportName = "Report";
            this.stiReport1.ReportSource = resources.GetString("stiReport1.ReportSource");
            this.stiReport1.ReportUnit = Stimulsoft.Report.StiReportUnitType.Centimeters;
            this.stiReport1.ScriptLanguage = Stimulsoft.Report.StiReportLanguageType.CSharp;
            this.stiReport1.UseProgressInThread = false;
            // 
            // dataSet11
            // 
            this.dataSet11.DataSetName = "DataSet1";
            this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // stiReportDataSource1
            // 
            this.stiReportDataSource1.Item = this.dataSet11;
            this.stiReportDataSource1.Name = "DataSet1";
            // 
            // SenId
            // 
            this.SenId.HeaderText = "SenarioId";
            this.SenId.Name = "SenId";
            this.SenId.ReadOnly = true;
            this.SenId.Visible = false;
            // 
            // TechId
            // 
            this.TechId.HeaderText = "TechnologyId";
            this.TechId.Name = "TechId";
            this.TechId.ReadOnly = true;
            this.TechId.Visible = false;
            // 
            // SenName
            // 
            this.SenName.FillWeight = 150F;
            this.SenName.HeaderText = "نام سناریو";
            this.SenName.Name = "SenName";
            this.SenName.ReadOnly = true;
            this.SenName.Width = 120;
            // 
            // TechnicalSpec
            // 
            this.TechnicalSpec.FillWeight = 150F;
            this.TechnicalSpec.HeaderText = "مشخصات فنی";
            this.TechnicalSpec.Name = "TechnicalSpec";
            this.TechnicalSpec.ReadOnly = true;
            this.TechnicalSpec.Width = 150;
            // 
            // FinancialAndEco
            // 
            this.FinancialAndEco.FillWeight = 200F;
            this.FinancialAndEco.HeaderText = "مشخصات مالی و اقتصادی";
            this.FinancialAndEco.Name = "FinancialAndEco";
            this.FinancialAndEco.ReadOnly = true;
            this.FinancialAndEco.Width = 170;
            // 
            // MarketSpec
            // 
            this.MarketSpec.FillWeight = 150F;
            this.MarketSpec.HeaderText = "مشخصات بازار";
            this.MarketSpec.Name = "MarketSpec";
            this.MarketSpec.ReadOnly = true;
            this.MarketSpec.Width = 150;
            // 
            // RulesAndApprovals
            // 
            this.RulesAndApprovals.FillWeight = 150F;
            this.RulesAndApprovals.HeaderText = "قوانین و تائیدیه ها";
            this.RulesAndApprovals.Name = "RulesAndApprovals";
            this.RulesAndApprovals.ReadOnly = true;
            this.RulesAndApprovals.Width = 150;
            // 
            // IsM
            // 
            this.IsM.HeaderText = "IsMain";
            this.IsM.Name = "IsM";
            this.IsM.ReadOnly = true;
            this.IsM.Visible = false;
            // 
            // Res
            // 
            this.Res.HeaderText = "Result";
            this.Res.Name = "Res";
            this.Res.ReadOnly = true;
            this.Res.Visible = false;
            // 
            // ResultStr
            // 
            this.ResultStr.FillWeight = 120F;
            this.ResultStr.HeaderText = "نتیجه نهایی";
            this.ResultStr.Name = "ResultStr";
            this.ResultStr.ReadOnly = true;
            this.ResultStr.Width = 120;
            // 
            // Status
            // 
            this.Status.FillWeight = 200F;
            this.Status.HeaderText = "وضعیت موقعیت تجاری سازی";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 190;
            // 
            // SenDetId
            // 
            this.SenDetId.HeaderText = "SenarioDetailId";
            this.SenDetId.Name = "SenDetId";
            this.SenDetId.ReadOnly = true;
            this.SenDetId.Visible = false;
            // 
            // FrmSenario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 754);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmSenario";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "خروجی برنامه";
            this.Load += new System.EventHandler(this.FrmSenario_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSenario_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTechnology.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTechnology;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnCreateSenario;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private Stimulsoft.Report.StiReport stiReport1;
        private DataSet1 dataSet11;
        private Stimulsoft.Report.Design.StiReportDataSource stiReportDataSource1;
        private System.Windows.Forms.MaskedTextBox txtQue1;
        private System.Windows.Forms.MaskedTextBox txtQue4;
        private System.Windows.Forms.MaskedTextBox txtQue3;
        private System.Windows.Forms.MaskedTextBox txtQue2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SenId;
        private System.Windows.Forms.DataGridViewTextBoxColumn TechId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SenName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TechnicalSpec;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinancialAndEco;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketSpec;
        private System.Windows.Forms.DataGridViewTextBoxColumn RulesAndApprovals;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Res;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultStr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn SenDetId;
    }
}