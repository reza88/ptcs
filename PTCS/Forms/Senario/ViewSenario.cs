﻿using System;
using System.Linq;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.Senario
{
    public class ViewSenario
    {
        private PTCSEntities _ptcsEntities;
        public ViewSenario()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            _ptcsEntities = new PTCSEntities();
            _ptcsEntities.Database.Connection.ConnectionString = strCnnString;
        }
        //public int AutoKey { get; set; }
        //public ViewTechnology(int _autoKey)
        //{
        //    AutoKey = _autoKey;
        //}
        public Guid GetLast()
        {
            return _ptcsEntities.Tbl_Senario
                .OrderByDescending(item => item.AutoKey)
                .Select(item => item.SenarioId).FirstOrDefault();
        }

        public Guid GetFirst()
        {
            return _ptcsEntities.Tbl_Senario
                .OrderBy(item => item.AutoKey)
                .Select(item => item.SenarioId).FirstOrDefault();
        }

        public Guid GetPrevious(int autoKey)
        {
            return _ptcsEntities.Tbl_Senario
                .Where(item => item.AutoKey < autoKey)
                .OrderByDescending(item => item.AutoKey)
                .Select(item => item.SenarioId).FirstOrDefault();
        }

        public Guid GetNext(int autoKey)
        {
           return _ptcsEntities.Tbl_Senario
                .Where(item => item.AutoKey > autoKey)
                .OrderBy(item => item.AutoKey)
                .Select(item => item.SenarioId).FirstOrDefault();
        }

        
    }
}
