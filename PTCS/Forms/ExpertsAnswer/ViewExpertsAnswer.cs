﻿using System;
using System.Linq;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.ExpertsAnswer
{
    class ViewExpertsAnswer
    {
        public PTCSEntities PtcsEntities;

        public ViewExpertsAnswer()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf",
              "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
        }
        public Guid GetLast()
        {
            return PtcsEntities.Tbl_ExpertOpinion
               .OrderByDescending(item => item.AutoKey)
               .Select(item => item.ExpertOpinionId).FirstOrDefault();
        }

        public Guid GetFirst()
        {
            return PtcsEntities.Tbl_ExpertOpinion
                 .OrderBy(item => item.AutoKey)
                 .Select(item => item.ExpertOpinionId).FirstOrDefault();
        }

        public Guid GetPrevious(int autoKey)
        {
            return PtcsEntities.Tbl_ExpertOpinion
                   .Where(item => item.AutoKey < autoKey)
                   .OrderByDescending(item => item.AutoKey)
                   .Select(item => item.ExpertOpinionId).FirstOrDefault();
        }

        public Guid GetNext(int autoKey)
        {
            return PtcsEntities.Tbl_ExpertOpinion
                   .Where(item => item.AutoKey > autoKey)
                   .OrderBy(item => item.AutoKey)
                   .Select(item => item.ExpertOpinionId).FirstOrDefault();
        }
    }
}
