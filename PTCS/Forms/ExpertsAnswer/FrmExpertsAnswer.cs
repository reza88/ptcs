﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Aspose.Cells;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;
using PTCSModel.ExpertsAnswers;

namespace PTCS.Forms.ExpertsAnswer
{
    public partial class FrmExpertsAnswer : XtraForm
    {
        readonly int _cNbTypist;
        //public string record_active;
        private Boolean _varAdding;
        private Boolean _varEditing;
        private int _autoKey;

        public Guid ExpertOpinionId { get; set; }

        public PTCSEntities PtcsEntities;

        public FrmExpertsAnswer(int nbTypist)
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;

            SkinManager.EnableFormSkins();
            _cNbTypist = nbTypist;

            InitializeComponent();
        }

        private void FrmExpertsAnswer_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo language = new System.Globalization.CultureInfo("fa-IR");
            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(language);

            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            proc_EnDis();

            var expert = PtcsEntities.Tbl_Expert
                .Where(i => i.Active == true)
                .Select(i => i.ExpertName)
                .ToList();
            foreach (var rec in expert)
            {
                cmbExpert.Properties.Items.Add(rec);
            }

            var technology = PtcsEntities.Tbl_Technology
                .Select(i => i.TechnologyName)
                .ToList();
            foreach (var rec in technology)
            {
                cmbTechnology.Properties.Items.Add(rec);
            }

            ViewExpertsAnswer vw = new ViewExpertsAnswer();

            ExpertOpinionId = vw.GetLast();

            if (ExpertOpinionId != null)
                proc_refresh();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            _varAdding = true;

            proc_empty();

            proc_EnDis();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            var query = PtcsEntities.Tbl_Typist
                .Where(i => i.nb == _cNbTypist)
                .Select(i => i.access_inprog)
                .FirstOrDefault();
            if (query != null)
            {

                if (query.Contains(",1-3-1,") || query.Contains("1-3-1,"))
                {
                    _varEditing = true;
                    proc_EnDis();
                    cmbExpert.Focus();
                    //record_active = txtNb.Text;
                }
                else
                    XtraMessageBox.Show("کاربر محترم شما مجوز ویرایش پاسخ خبره را ندارید", "آگهی",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbExpert.Text != string.Empty)
            {

                var expertId = PtcsEntities.Tbl_Expert
                    .Where(i => i.Active == true && i.ExpertName == cmbExpert.Text)
                    .Select(i => i.ExpertId)
                    .FirstOrDefault();
                var technologyId = PtcsEntities.Tbl_Technology
                    .Where(i => i.TechnologyName == cmbTechnology.Text)
                    .Select(i => i.TechnologyId)
                    .FirstOrDefault();

                if (_varAdding && _varEditing == false)
                {
                    bool duplicateExists = PtcsEntities.Tbl_ExpertOpinion
                        .Any(i => i.TechnologyId == technologyId && i.ExpertId == expertId);
                    if (!duplicateExists)
                    {
                        var result = PtcsEntities.Tbl_ExpertOpinion.Add(new Tbl_ExpertOpinion
                        {
                            ExpertOpinionId = Guid.NewGuid(),
                            ExpertId = expertId,
                            TechnologyId = technologyId
                        });

                        PtcsEntities.SaveChanges();

                        if (result != null && result.ExpertOpinionId != Guid.Empty)
                        {
                            ExpertOpinionId = result.ExpertOpinionId;
                            proc_refresh();
                        }
                        else
                            XtraMessageBox.Show("خطا در ثبت پاسخ خبره", "خطا", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                        _varAdding = false;
                        _varEditing = false;
                        proc_EnDis();
                    }
                    else
                        XtraMessageBox.Show("پاسخ خبره با این فناوری تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);



                }


                else if (_varAdding == false && _varEditing)
                {
                    bool duplicateExists = PtcsEntities.Tbl_ExpertOpinion
                        .Any(i => i.ExpertOpinionId != ExpertOpinionId && i.TechnologyId == technologyId && i.ExpertId == expertId);

                    if (!duplicateExists)
                    {
                        var updateQuery = PtcsEntities.Tbl_ExpertOpinion
                            .Where(i => i.ExpertOpinionId == ExpertOpinionId)
                            .Select(i => i)
                            .FirstOrDefault();
                        if (updateQuery != null)
                        {

                            updateQuery.TechnologyId = technologyId;
                            updateQuery.ExpertId = expertId;


                            PtcsEntities.SaveChanges();

                            _varAdding = false;
                            _varEditing = false;
                            proc_EnDis();
                            proc_refresh();
                        }
                    }

                    else
                        XtraMessageBox.Show("پاسخ خبره با این فناوری تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                }
            }
            else
                XtraMessageBox.Show("اطلاعات را کامل وارد کنید", "اخطار");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _varAdding = false;
            _varEditing = false;
            proc_EnDis();
            if (ExpertOpinionId != null)
                proc_refresh();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DataSet2 ds = new DataSet2();

            var titr1 = PtcsEntities.Tbl_Parameters.Where(a => a.name_variable == "titr1")
                            .Select(a => a.data_variable)
                            .FirstOrDefault();

            var titr2 = PtcsEntities.Tbl_Parameters.Where(a => a.name_variable == "titr2")
                        .Select(a => a.data_variable)
                        .FirstOrDefault();

            if (ExpertOpinionId != null)
            {
                var allOpinions = (from a in PtcsEntities.Tbl_ExpertOpinionDetail
                                   join b in PtcsEntities.Tbl_ExpertOpinion on a.ExpertOpinionId equals b.ExpertOpinionId
                                   join c in PtcsEntities.Tbl_Component on a.ComponentId equals c.ComponentId
                                   join d in PtcsEntities.Tbl_QuestionnaireDimension on c.QuestionnaireDimensionId equals d.QuestionnaireDimensionId
                                   where a.ExpertOpinionId == ExpertOpinionId


                                   select new { a, c, d }).Select(item => new ExpertsAnswerModel
                                   {
                                       QuestionnaireDimensionAutoKey = item.d.AutoKey,
                                       ComponentAutoKey = item.c.AutoKey,
                                       QuestionnaireDimensionId = item.d.QuestionnaireDimensionId,
                                       QuestionnaireDimensionName = item.d.QuestionnaireDimensionName,
                                       ComponentName = item.c.ComponentName,
                                       ComponentDescription = item.c.ComponentDescription,
                                       ComponentWeight = item.c.ComponentWeight,
                                       IsDominant = item.c.IsDominant,
                                       Opinion = item.a.Opinion
                                   })
                                   .OrderBy(item => item.QuestionnaireDimensionAutoKey)
                                   .ThenBy(item2 => item2.ComponentAutoKey)
                                   .ToList();


                //DataTable dt = new DataTable();

                //dt.Columns.Add("Id");
                //dt.Columns.Add("Name");
                //dt.Columns.Add("Wieght");
                //dt.Columns.Add("IsDominate");

                //DataRow dr = dt.NewRow();
                if (allOpinions.Count > 0)
                {
                    for (int i = 0; i < allOpinions.Count(); i++)
                    {

                        int n = dataGridView1.Rows.Add();

                        if (i == 0)
                        {
                            ds.DataTable1.AddDataTable1Row(
                                allOpinions[i].QuestionnaireDimensionName,
                                allOpinions[i].ComponentName,
                                allOpinions[i].ComponentDescription,
                                allOpinions[i].Opinion.ToString());

                        }

                        else if (allOpinions[i].QuestionnaireDimensionName == allOpinions[i - 1].QuestionnaireDimensionName)
                        {
                            ds.DataTable1.AddDataTable1Row(
                                "",
                                allOpinions[i].ComponentName,
                                allOpinions[i].ComponentDescription,
                                allOpinions[i].Opinion.ToString());
                        }
                        else
                        {
                            ds.DataTable1.AddDataTable1Row(
                               allOpinions[i].QuestionnaireDimensionName,
                               allOpinions[i].ComponentName,
                               allOpinions[i].ComponentDescription,
                               allOpinions[i].Opinion.ToString());
                        }




                    }
                    if (ds.DataTable1 != null)
                    {
                        //stiReport1.ClearAllStates();

                        stiReport1.Dictionary.DataStore.Clear();
                        stiReport1.Dictionary.Databases.Clear();
                        stiReport1.Dictionary.DataSources.Clear();
                        stiReport1.BusinessObjectsStore.Clear();

                        stiReport1.RegData(ds);
                        stiReport1.Dictionary.Synchronize();

                        stiReport1.Compile();

                        //stiReport1.RegData(ds);

                        stiReport1["Titr1"] = titr1;
                        stiReport1["Titr2"] = titr2;
                        stiReport1["TechnologyName"] = cmbTechnology.Text.Trim();
                        stiReport1["ExpertName"] = cmbExpert.Text.Trim();
                        stiReport1["date_print"] = proc_datetime_persian(DateTime.Now).Substring(0, 10);


                        stiReport1.Render();
                        stiReport1.Show(false);
                    }
                }

            }
        }

        public string proc_datetime_persian(DateTime datetime_miladi)
        {
            System.Globalization.PersianCalendar pc = new System.Globalization.PersianCalendar();
            string dt_per = pc.GetYear(datetime_miladi).ToString().Substring(0, 4) + "/" +
                pc.GetMonth(datetime_miladi).ToString().PadLeft(2, '0') + "/" +
                pc.GetDayOfMonth(datetime_miladi).ToString().PadLeft(2, '0') + " " +
                pc.GetHour(datetime_miladi).ToString().PadLeft(2, '0') + ":" +
                pc.GetMinute(datetime_miladi).ToString().PadLeft(2, '0') + ":" +
                pc.GetSecond(datetime_miladi).ToString().PadLeft(2, '0');
            return dt_per;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbExpert_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{tab}");
            if (e.KeyCode == Keys.Right)
                SendKeys.Send("+{tab}");
        }

        private void FrmExpertsAnswer_KeyDown(object sender, KeyEventArgs e)
        {

            ViewExpertsAnswer vw = new ViewExpertsAnswer();

            if (e.KeyCode == Keys.F5 && e.Modifiers == Keys.None)
            {
                btnPrint_Click(sender, e);

            }


            if (!_varAdding && !_varEditing)
            {
                #region previous

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.None)
                {

                    ExpertOpinionId = vw.GetPrevious(_autoKey);

                    if (ExpertOpinionId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("اولین پرونده است");
                }

                #endregion

                #region Next

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.None)
                {

                    ExpertOpinionId = vw.GetNext(_autoKey);

                    if (ExpertOpinionId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("آخرین پرونده است");

                }

                #endregion

                #region First

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.Control)
                {
                    ExpertOpinionId = vw.GetFirst();

                    if (ExpertOpinionId != null)
                        proc_refresh();
                }

                #endregion

                #region Last

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Control)
                {
                    ExpertOpinionId = vw.GetLast();

                    if (ExpertOpinionId != null)
                        proc_refresh();

                }

                #endregion

            }
        }

        private void proc_empty()
        {
            cmbExpert.SelectedIndex = -1;
            cmbTechnology.SelectedIndex = -1;

            cmbExpert.Focus();
        }

        public void proc_EnDis()
        {
            if (_varAdding)
            {
                cmbExpert.Enabled = true;
                cmbTechnology.Enabled = true;

                btnNew.Enabled = false;
                btnEdit.Enabled = false;
                btnPrint.Enabled = false;
                btnImport.Enabled = false;
                btnExit.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;

            }
            else if (_varEditing)
            {
                cmbExpert.Enabled = true;
                cmbTechnology.Enabled = true;

                btnNew.Enabled = false;
                btnEdit.Enabled = false;
                btnPrint.Enabled = false;
                btnImport.Enabled = false;
                btnExit.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;
            }
            else
            {
                cmbExpert.Enabled = false;
                cmbTechnology.Enabled = false;

                btnNew.Enabled = true;
                btnEdit.Enabled = true;
                btnPrint.Enabled = true;
                btnImport.Enabled = true;
                btnExit.Enabled = true;
                btnSave.Enabled = false;
                btnCancel.Enabled = false;
            }
        }

        public void proc_refresh()
        {


            if (ExpertOpinionId != null)
            {

                var query = from a in PtcsEntities.Tbl_ExpertOpinion
                            join b in PtcsEntities.Tbl_Expert on a.ExpertId equals b.ExpertId
                            join c in PtcsEntities.Tbl_Technology on a.TechnologyId equals c.TechnologyId
                            where a.ExpertOpinionId == ExpertOpinionId
                            select new { a, b, c };
                if (query.Count() != 0)
                {
                    var rec = query.FirstOrDefault();
                    if (rec != null)
                    {
                        _autoKey = rec.a.AutoKey;
                        ExpertOpinionId = rec.a.ExpertOpinionId;
                        cmbExpert.Text = rec.b.ExpertName;
                        cmbTechnology.Text = rec.c.TechnologyName;
                        //txtTechnologyDescription.Text = rec.TechnologyDescription;
                    }


                    proc_GetOpinions();


                }
            }
        }

        public void proc_GetOpinions()
        {

            if (ExpertOpinionId != null)
            {
                var allOpinions = (from a in PtcsEntities.Tbl_ExpertOpinionDetail
                                   join b in PtcsEntities.Tbl_ExpertOpinion on a.ExpertOpinionId equals b.ExpertOpinionId
                                   join c in PtcsEntities.Tbl_Component on a.ComponentId equals c.ComponentId
                                   join d in PtcsEntities.Tbl_QuestionnaireDimension on c.QuestionnaireDimensionId equals d.QuestionnaireDimensionId
                                   where a.ExpertOpinionId == ExpertOpinionId


                                   select new { a, c, d }).Select(item => new ExpertsAnswerModel
                                   {
                                       QuestionnaireDimensionAutoKey = item.d.AutoKey,
                                       ComponentAutoKey = item.c.AutoKey,
                                       QuestionnaireDimensionId = item.d.QuestionnaireDimensionId,
                                       QuestionnaireDimensionName = item.d.QuestionnaireDimensionName,
                                       ComponentName = item.c.ComponentName,
                                       ComponentDescription = item.c.ComponentDescription,
                                       ComponentWeight = item.c.ComponentWeight,
                                       IsDominant = item.c.IsDominant,
                                       Opinion = item.a.Opinion
                                   })
                                   .OrderBy(item => item.QuestionnaireDimensionAutoKey)
                                   .ThenBy(item2 => item2.ComponentAutoKey)
                                   .ToList();


                //DataTable dt = new DataTable();

                //dt.Columns.Add("Id");
                //dt.Columns.Add("Name");
                //dt.Columns.Add("Wieght");
                //dt.Columns.Add("IsDominate");

                //DataRow dr = dt.NewRow();
                if (allOpinions.Count > 0)
                {
                    dataGridView1.Rows.Clear();
                    for (int i = 0; i < allOpinions.Count(); i++)
                    {

                        int n = dataGridView1.Rows.Add();

                        if (i == 0)
                        {
                            dataGridView1.Rows[n].Cells[0].Value = allOpinions[i].QuestionnaireDimensionName;
                            dataGridView1.Rows[n].Cells[1].Value = allOpinions[i].ComponentName;
                            dataGridView1.Rows[n].Cells[2].Value = allOpinions[i].ComponentDescription;
                            dataGridView1.Rows[n].Cells[3].Value = allOpinions[i].Opinion;
                        }

                        else if (allOpinions[i].QuestionnaireDimensionName == allOpinions[i - 1].QuestionnaireDimensionName)
                        {
                            dataGridView1.Rows[n].Cells[0].Value = "";
                            dataGridView1.Rows[n].Cells[1].Value = allOpinions[i].ComponentName;
                            dataGridView1.Rows[n].Cells[2].Value = allOpinions[i].ComponentDescription;
                            dataGridView1.Rows[n].Cells[3].Value = allOpinions[i].Opinion;
                        }
                        else
                        {
                            dataGridView1.Rows[n].Cells[0].Value = allOpinions[i].QuestionnaireDimensionName;
                            dataGridView1.Rows[n].Cells[1].Value = allOpinions[i].ComponentName;
                            dataGridView1.Rows[n].Cells[2].Value = allOpinions[i].ComponentDescription;
                            dataGridView1.Rows[n].Cells[3].Value = allOpinions[i].Opinion;
                        }



                        //dt.Rows.Add(rec.ComponentId, rec.ComponentName, rec.ComponentWeight, rec.IsDominant);
                        //dr["Id"] = rec.ComponentId;
                        //dr["Name"] = rec.ComponentName;
                        //dr["Wieght"] = rec.ComponentWeight;
                        //dr["IsDominate"] = rec.IsDominant;
                        //dt.Rows.Add(dr);
                    }
                    //dataGridView2.DataSource = dt;
                    dataGridView1.Columns[1].Frozen = true;
                }
                else
                {
                    dataGridView1.DataSource = null;
                    dataGridView1.Rows.Clear();
                }
            }
        }


        private void btnImport_Click(object sender, EventArgs e)
        {
            var query = PtcsEntities.Tbl_Typist
                .Where(i => i.active && i.nb == _cNbTypist)
                .Select(i => i.access_inprog)
                .FirstOrDefault();

            if (query != null)
            {

                if (query.Contains(",1-3-2,") || query.Contains("1-3-2,"))
                {

                    OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "Excel Files(.xlsx)| *.xlsx";
                    openFile.RestoreDirectory = true;
                    openFile.Title = "Browse Text Files";
                    openFile.CheckFileExists = true;
                    openFile.CheckPathExists = true;

                    var insertNumber = 0;

                    var tecknologyId = PtcsEntities.Tbl_Technology
                        .Where(i => i.TechnologyName == cmbTechnology.Text)
                        .Select(i => i.TechnologyId)
                        .FirstOrDefault();

                    if (openFile.ShowDialog() == DialogResult.OK)
                    {

                        Cursor = Cursors.WaitCursor;

                        Workbook wb = new Workbook(openFile.FileName);
                        Worksheet worksheet1 = wb.Worksheets[0];
                        Worksheet worksheet2 = wb.Worksheets[1];

                        Cells cells2 = worksheet2.Cells;

                        if (Guid.Parse(cells2["A1000"].Value.ToString()) == tecknologyId)
                        {

                            Guid? questionnaireDimensionId = null;

                            Cells cells1 = worksheet1.Cells;

                            List<ExpertsAnswerModel> myList = new List<ExpertsAnswerModel>();
                            var aa = wb.Worksheets[0].Cells.MaxDataRow + 1;
                            var bb = wb.Worksheets[0].Cells.MaxDataColumn + 1;
                            var db = wb.Worksheets[0].Cells.ExportDataTable(2, 0, aa, bb);
                            DataTable dt = new DataTable();
                            dt = db;
                            //DataTable dt = wb.Worksheets[0].Cells.ExportDataTable(2, 0,aa, bb);


                            //int col = CellsHelper.ColumnNameToIndex("AA");

                            //int last_row = worksheet1.Cells.GetLastDataRow(col);
                            //for (int i = 8; i <= last_row; i++)
                            //{
                            //    myList.Add(cells1[i, col].Value.ToString());
                            //}
                            int opinionCount = 0;
                            if (dt != null)
                            {
                                for (int i = 0; i <= dt.Rows.Count - 3; i++)
                                {
                                    if (dt.Rows[i][3].ToString() != "")
                                        opinionCount++;
                                }
                                if (opinionCount > 0)
                                {
                                    for (int i = 0; i <= dt.Rows.Count - 3; i++)
                                    {

                                        var questionnaireDimensionName = dt.Rows[i][0].ToString();

                                        Guid? result1 = PtcsEntities.Tbl_QuestionnaireDimension
                                            .Where(a => a.QuestionnaireDimensionName == questionnaireDimensionName)
                                            .Select(a => a.QuestionnaireDimensionId)
                                            .FirstOrDefault();

                                        if (result1 != Guid.Empty)
                                            questionnaireDimensionId = result1;

                                        var componentName = dt.Rows[i][1].ToString();

                                        var componentId = (from a in PtcsEntities.Tbl_Component
                                                           join b in PtcsEntities.Tbl_Technology on a.TechnologyId equals
                                                           b.TechnologyId
                                                           join c in PtcsEntities.Tbl_QuestionnaireDimension on
                                                           a.QuestionnaireDimensionId equals
                                                           c.QuestionnaireDimensionId

                                                           where b.TechnologyId == tecknologyId &&
                                                                 c.QuestionnaireDimensionId == questionnaireDimensionId &&
                                                                 a.ComponentName == componentName
                                                           select a.ComponentId).FirstOrDefault();

                                        if (componentId != null)
                                        {
                                            bool isExist = PtcsEntities.Tbl_ExpertOpinionDetail.Any(a => a.ExpertOpinionId == ExpertOpinionId &&
                                                         a.ComponentId == componentId);
                                            if (!isExist)
                                            {
                                                var res =
                                                    PtcsEntities.Tbl_ExpertOpinionDetail.Add(new Tbl_ExpertOpinionDetail
                                                    {
                                                        ExpertOpinionDetailId = Guid.NewGuid(),
                                                        ExpertOpinionId = ExpertOpinionId,
                                                        ComponentId = componentId,
                                                        Opinion = Convert.ToByte(dt.Rows[i][3].ToString().PadLeft(1, '0'))
                                                    });
                                                PtcsEntities.SaveChanges();
                                                if (res.ExpertOpinionDetailId != Guid.Empty)
                                                {
                                                    insertNumber++;
                                                }
                                            }
                                            else
                                            {
                                                var res = PtcsEntities.Tbl_ExpertOpinionDetail
                                                        .Where(a => a.ExpertOpinionId == ExpertOpinionId &&
                                                                   a.ComponentId == componentId)
                                                        .Select(a => a)
                                                        .FirstOrDefault();
                                                if (res != null)
                                                {
                                                    res.Opinion = Convert.ToByte(dt.Rows[i][3].ToString().PadLeft(1, '0'));
                                                    PtcsEntities.SaveChanges();
                                                    insertNumber++;
                                                }
                                            }
                                        }

                                    }

                                    if (insertNumber == 0)
                                        XtraMessageBox.Show("نظرات این خبره قبلا وارد شده است", "خطا",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);

                                    Cursor = Cursors.Default;
                                    proc_GetOpinions();


                                }
                                else
                                {
                                    XtraMessageBox.Show("فایل مورد نظر فاقد نظر خبره می باشد", "خطا",
                                          MessageBoxButtons.OK,
                                          MessageBoxIcon.Error);
                                    Cursor = Cursors.Default;
                                }
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show("فناوری موجود با فایل مورد نظر تطابق ندارد", "خطا", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            Cursor = Cursors.Default;
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("کاربر محترم شما مجوز وارد کردن پاسخ خبره را ندارید", "آگهی",
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Cursor = Cursors.Default;
                }
            }

        }
    }
}
