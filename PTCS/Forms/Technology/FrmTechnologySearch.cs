﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.Technology
{
    public partial class FrmTechnologySearch : XtraForm
    {
        public PTCSEntities PtcsEntities;
        private Tbl_Expert _tblExpert;

        public Guid Ss;
        public FrmTechnologySearch()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            _tblExpert = new Tbl_Expert();
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            proc_find();
        }

        private void proc_find()
        {

            //var query = ptcsEntities.Tbl_Expert.AsQueryable();

            // if (string.IsNullOrWhiteSpace(txtExpertName.Text.Trim()) == false)
            //     query.Where(item => item.ExpertName.Contains(txtExpertName.Text.Trim()));


            // dataGridView1.DataSource=query.Select(item => new{
            //                                                     Id = item.ExpertId,
            //                                                     Name = item.ExpertName
            //                                                  }).ToList();

            var query = (from i in PtcsEntities.Tbl_Technology
                         where
                         txtTechnologyName.Text.Trim() != "" ? i.TechnologyName.Contains(txtTechnologyName.Text.Trim()) : true

                         select new
                         {
                             Id = i.TechnologyId,
                             Name = i.TechnologyName,
                             //active = i.Active == true ? "فعال" : "غیر فعال"
                         }).ToList();
            if (query.Count() > 0)
            {
                dataGridView1.DataSource = query;

                dataGridView1.Columns[0].Visible = false;

                dataGridView1.Columns[1].HeaderText = "نام فناوری";
                dataGridView1.Columns[1].Width = 550;
                //dataGridView1.Columns[2].HeaderText = "وضعیت";
                //dataGridView1.Columns[2].Width = 100;
            }


        }

        private void FrmTechnologySearch_Load(object sender, EventArgs e)
        {
            proc_find();
        }

        private void txtExpertName_TextChanged(object sender, EventArgs e)
        {
            proc_find();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView1.DataSource != null)
            {
                Ss = Guid.Parse(dataGridView1.Rows[(sender as DataGridView).CurrentCell.RowIndex].Cells[0].Value.ToString());

                this.Close();

            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.DataSource != null)
                {
                    Ss = Guid.Parse(dataGridView1.Rows[(sender as DataGridView).CurrentCell.RowIndex].Cells[0].Value.ToString());

                    this.Close();

                }
            }
        }
    }
}
