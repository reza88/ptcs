﻿using System;
using System.Linq;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.Technology
{
    public class ViewTechnology
    {
        public PTCSEntities PtcsEntities;

        public ViewTechnology()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf",
                "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
        }

        public Guid GetLast()
        {
         return PtcsEntities.Tbl_Technology
                .OrderByDescending(item => item.AutoKey)
                .Select(item => item.TechnologyId).FirstOrDefault();
        }

        public Guid GetFirst()
        {
            return PtcsEntities.Tbl_Technology
                .OrderBy(item => item.AutoKey)
                .Select(item => item.TechnologyId).FirstOrDefault();
        }

        public Guid GetPrevious(int autoKey)
        {
           return PtcsEntities.Tbl_Technology
                .Where(item => item.AutoKey < autoKey)
                .OrderByDescending(item => item.AutoKey)
                .Select(item => item.TechnologyId).FirstOrDefault();
        }

        public Guid GetNext(int autoKey)
        {
          return PtcsEntities.Tbl_Technology
                .Where(item => item.AutoKey > autoKey)
                .OrderBy(item => item.AutoKey)
                .Select(item => item.TechnologyId).FirstOrDefault();
        }

        
    }
}
