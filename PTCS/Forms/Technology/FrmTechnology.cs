﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Aspose.Cells;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;
using PTCSModel.TechnologyModel;

namespace PTCS.Forms.Technology
{
    public partial class FrmTechnology : XtraForm
    {
        readonly int _cNbTypist;
        //public string record_active;
        private bool _varAdding;
        private bool _varEditing;
        private int _autoKey;
        private Guid TechnologyId { get; set; }
        private Guid QuestionnaireDimensionId { get; set; }
        private List<AllComponent> AllComponents { get; set; }
        private bool _componentStatus;

        public PTCSEntities PtcsEntities;
        public FrmTechnology(int nbTypist)
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;

            SkinManager.EnableFormSkins();
            _cNbTypist = nbTypist;

            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            _varAdding = true;

            proc_empty();

            proc_EnDis();
            txtThreshold.Text = "2.5";
            txtTechnologyName.Focus();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var query = PtcsEntities.Tbl_Typist
                .Where(i => i.nb == _cNbTypist)
                .Select(i => i.access_inprog)
                .FirstOrDefault();

            if (query != null)
            {

                if (query.Contains(",1-1-1,") || query.Contains("1-1-1,"))
                {
                    _varEditing = true;
                    proc_EnDis();
                    txtTechnologyName.Focus();
                    //record_active = txtNb.Text;
                }
                else
                    XtraMessageBox.Show("کاربر محترم شما مجوز ویرایش اطلاعات فن آوری را ندارید", "آگهی",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _varAdding = false;
            _varEditing = false;
            proc_EnDis();
            if (TechnologyId != null)
                proc_refresh();


        }

        private void proc_empty()
        {
            txtTechnologyName.Text = "";
            txtTechnologyDescription.Text = "";

            txtTechnologyName.Focus();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmTechnology_KeyDown(object sender, KeyEventArgs e)
        {

            ViewTechnology vw = new ViewTechnology();

            if (e.KeyCode == Keys.F5 && e.Modifiers == Keys.None)
            {
                btnPrint_Click(sender, e);

            }

            if (e.KeyCode == Keys.F2 && e.Modifiers == Keys.None)
            {
                if (_componentStatus)
                {
                    _componentStatus = false;
                    proc_Component();
                }
                else
                {
                    _componentStatus = true;
                    proc_Component();
                }
            }

            if (!_varAdding && !_varEditing)
            {
                #region previous

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.None)
                {

                    TechnologyId = vw.GetPrevious(_autoKey);

                    if (TechnologyId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("اولین پرونده است");
                }

                #endregion

                #region Next

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.None)
                {

                    TechnologyId = vw.GetNext(_autoKey);

                    if (TechnologyId != null)
                        proc_refresh();

                    else
                        XtraMessageBox.Show("آخرین پرونده است");

                }

                #endregion

                #region First

                if (e.KeyCode == Keys.F3 && e.Modifiers == Keys.Control)
                {
                    TechnologyId = vw.GetFirst();

                    if (TechnologyId != null)
                        proc_refresh();
                }

                #endregion

                #region Last

                if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Control)
                {
                    TechnologyId = vw.GetLast();

                    if (TechnologyId != null)
                        proc_refresh();

                }

                #endregion




            }
        }

        private void txtTechnologyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{tab}");
            if (e.KeyCode == Keys.Right)
                SendKeys.Send("+{tab}");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void frmTechnology_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo language = new System.Globalization.CultureInfo("fa-IR");
            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(language);

            proc_EnDis();


            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            var questionnaireDimension = PtcsEntities.Tbl_QuestionnaireDimension
                .Select(a => new
                {
                    a.AutoKey,
                    a.QuestionnaireDimensionId,
                    a.QuestionnaireDimensionName
                })
               .OrderBy(item => item.AutoKey)
               .ToList();
            foreach (var rec in questionnaireDimension)
            {
                cmbQuestionnaireDimension.Properties.Items.Add(rec.QuestionnaireDimensionName);
            }
            cmbQuestionnaireDimension.SelectedIndex = 0;

            ViewTechnology vw = new ViewTechnology();

            TechnologyId = vw.GetLast();

            if (TechnologyId != null)
                proc_refresh();

            _componentStatus = true;
            if (_componentStatus)
            {
                _componentStatus = false;
                proc_Component();
            }
            else
            {
                _componentStatus = true;
                proc_Component();
            }



            //  procGetComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtTechnologyName.Text != string.Empty)
            {

                if (_varAdding && _varEditing == false)
                {
                    bool insertDuplicateExists = PtcsEntities.Tbl_Technology.Any(x => x.TechnologyName == txtTechnologyName.Text.Trim());
                    if (!insertDuplicateExists)
                    {
                        var result = PtcsEntities.Tbl_Technology.Add(new Tbl_Technology
                        {
                            TechnologyId = Guid.NewGuid(),
                            TechnologyName = txtTechnologyName.Text.Trim(),
                            //  LowThreshold = Convert.ToDecimal(txtThreshold.Text.Trim().Replace('.', '/')),
                            LowThreshold = Convert.ToDecimal(txtThreshold.Text.Trim()),
                            TechnologyDescription = txtTechnologyDescription.Text.Trim()
                        });

                        if (result != null)
                        {
                            PtcsEntities.SaveChanges();
                            TechnologyId = result.TechnologyId;
                            var defaultComponent = PtcsEntities.Tbl_DefaultComponent.ToList();
                            foreach (var rec in defaultComponent)
                            {
                                PtcsEntities.Tbl_Component.Add(new Tbl_Component
                                {
                                    ComponentId = Guid.NewGuid(),
                                    TechnologyId = TechnologyId,
                                    QuestionnaireDimensionId = rec.QuestionnaireDimensionId,
                                    ComponentDescription = rec.ComponentDescription,
                                    ComponentName = rec.ComponentName,
                                    ComponentWeight = rec.ComponentWeight,
                                    IsDominant = rec.IsDominant
                                });
                                PtcsEntities.SaveChanges();
                            }
                            proc_refresh();
                        }
                        else
                            XtraMessageBox.Show("خطا در ثبت فن آوری", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        XtraMessageBox.Show("فن آوری تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                    _varAdding = false;
                    _varEditing = false;
                    proc_EnDis();


                }


                else if (_varAdding == false && _varEditing)
                {

                    bool duplicateExists = PtcsEntities.Tbl_Technology.Any(x => x.TechnologyId != TechnologyId && x.TechnologyName == txtTechnologyName.Text.Trim());
                    if (!duplicateExists)
                    {

                        var updateQuery = PtcsEntities.Tbl_Technology
                            .Where(a => a.TechnologyId == TechnologyId)
                            .Select(a => a)
                            .FirstOrDefault();
                        if (updateQuery != null)
                        {
                            updateQuery.TechnologyName = txtTechnologyName.Text.Trim();
                            updateQuery.TechnologyDescription = txtTechnologyDescription.Text.Trim();
                            //updateQuery.LowThreshold = Convert.ToDecimal(txtThreshold.Text.Trim().Replace('.', '/'));
                            updateQuery.LowThreshold = Convert.ToDecimal(txtThreshold.Text.Trim());
                            PtcsEntities.SaveChanges();

                            _varAdding = false;
                            _varEditing = false;
                            proc_EnDis();
                            proc_refresh();
                        }
                    }

                    else
                        XtraMessageBox.Show(" فن آوری تکراری می باشد", "اخطار", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                }
            }
            else
                XtraMessageBox.Show("اطلاعات را کامل وارد کنید", "اخطار");
        }

        public void proc_EnDis()
        {
            if (_varAdding)
            {
                txtTechnologyName.Enabled = true;
                txtThreshold.Enabled = true;
                txtTechnologyDescription.Enabled = true;

                btnNew.Enabled = false;
                btnEdit.Enabled = false;
                btnExportExcel.Enabled = false;
                btnExit.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;

            }
            else if (_varEditing)
            {
                txtTechnologyName.Enabled = true;
                txtThreshold.Enabled = true;
                txtTechnologyDescription.Enabled = true;

                btnNew.Enabled = false;
                btnEdit.Enabled = false;
                btnExportExcel.Enabled = false;
                btnExit.Enabled = false;
                btnSave.Enabled = true;
                btnCancel.Enabled = true;
            }
            else
            {
                txtTechnologyName.Enabled = false;
                txtThreshold.Enabled = false;
                txtTechnologyDescription.Enabled = false;

                btnNew.Enabled = true;
                btnEdit.Enabled = true;
                btnExportExcel.Enabled = true;
                btnExit.Enabled = true;
                btnSave.Enabled = false;
                btnCancel.Enabled = false;
            }
        }

        public void proc_refresh()
        {

            if (TechnologyId != Guid.Empty)
            {
                var query = PtcsEntities.Tbl_Technology
                     .Where(a => a.TechnologyId == TechnologyId)
                     .OrderBy(a => a.AutoKey)
                     .Select(a => a)
                     .FirstOrDefault();

                if (query != null)
                {
                    _autoKey = query.AutoKey;
                    TechnologyId = query.TechnologyId;
                    txtTechnologyName.Text = query.TechnologyName;
                    txtTechnologyDescription.Text = query.TechnologyDescription;
                    txtThreshold.Text = query.LowThreshold.ToString();

                    ProcGetComponent();

                }
            }
        }

        public void proc_Component()
        {
            if (_componentStatus)
                groupControl2.Enabled = true;
            else
                groupControl2.Enabled = false;
        }

        public void ProcGetComponent()
        {
            if (TechnologyId != null)
            {
                AllComponents = (from i in PtcsEntities.Tbl_Component
                                 join j in PtcsEntities.Tbl_QuestionnaireDimension on i.QuestionnaireDimensionId equals j.QuestionnaireDimensionId

                                 where i.TechnologyId == TechnologyId &&
                                       j.QuestionnaireDimensionName == cmbQuestionnaireDimension.Text

                                 select new { i, j }).Select(a => new AllComponent
                                 {
                                     Autokey = a.i.AutoKey,
                                     TechnologyId = a.i.TechnologyId,
                                     QuestionnaireDimensionId = a.i.QuestionnaireDimensionId,
                                     QuestionnaireDimensionName = a.j.QuestionnaireDimensionName,
                                     ComponentId = a.i.ComponentId,
                                     ComponentName = a.i.ComponentName,
                                     ComponentDescription = a.i.ComponentDescription,
                                     ComponentWeight = a.i.ComponentWeight,
                                     IsDominant = a.i.IsDominant
                                 })
                                 .OrderBy(item => item.Autokey)
                                 .ToList();


                //DataTable dt = new DataTable();

                //dt.Columns.Add("Id");
                //dt.Columns.Add("Name");
                //dt.Columns.Add("Wieght");
                //dt.Columns.Add("IsDominate");

                //DataRow dr = dt.NewRow();
                if (AllComponents != null)
                {
                    int row = 1;
                    dataGridView1.Rows.Clear();
                    foreach (var rec in AllComponents)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = rec.ComponentId;
                        dataGridView1.Rows[n].Cells[1].Value = row++;
                        dataGridView1.Rows[n].Cells[2].Value = rec.ComponentName;
                        dataGridView1.Rows[n].Cells[3].Value = rec.ComponentDescription;
                        dataGridView1.Rows[n].Cells[4].Value = rec.ComponentWeight;
                        dataGridView1.Rows[n].Cells[5].Value = rec.IsDominant;

                        //dt.Rows.Add(rec.ComponentId, rec.ComponentName, rec.ComponentWeight, rec.IsDominant);
                        //dr["Id"] = rec.ComponentId;
                        //dr["Name"] = rec.ComponentName;
                        //dr["Wieght"] = rec.ComponentWeight;
                        //dr["IsDominate"] = rec.IsDominant;
                        //dt.Rows.Add(dr);
                    }
                    //dataGridView2.DataSource = dt;
                }
            }
        }

        private void cmbQuestionnaireDimension_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProcGetComponent();
        }




        private class Weights
        {
            public decimal Weigth { get; set; }
        }
        private void btnSaveComponent_Click(object sender, EventArgs e)
        {
            decimal componentWeight;
            bool isDominant;
            int isDominantCount = 0;
            try
            {
                for (int i = 0; i <= dataGridView1.RowCount - 2; i++)
                {
                    if (Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value) == 0)
                    {
                        XtraMessageBox.Show("وزن یکی از مولفه ها صفر است", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (dataGridView1.Rows[i].Cells[5].Value != null && (bool)dataGridView1.Rows[i].Cells[5].Value)
                        isDominantCount++;


                }

                if (isDominantCount == 0)
                {
                    XtraMessageBox.Show("مولفه غالب انتخاب نشده است", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                QuestionnaireDimensionId = (from i in PtcsEntities.Tbl_QuestionnaireDimension
                                            where i.QuestionnaireDimensionName == cmbQuestionnaireDimension.Text
                                            select i.QuestionnaireDimensionId).FirstOrDefault();


                if (TechnologyId != null)
                {
                    if (dataGridView1.RowCount > 0)
                    {
                        #region DeleteData


                        var query2 = (from i in PtcsEntities.Tbl_Component
                                     where
                                     i.TechnologyId == TechnologyId &&
                                     i.QuestionnaireDimensionId == QuestionnaireDimensionId

                                     select i).ToList();
                        if (query2.Count > 0)
                        {
                            foreach (var rec2 in query2)
                            {
                                PtcsEntities.Tbl_Component.Remove(rec2);
                            }

                            PtcsEntities.SaveChanges();
                        }

                        #endregion

                        #region SaveData

                        if (dataGridView1.RowCount > 0)
                        {
                            for (int i = 0; i <= (dataGridView1.RowCount - 2); i++)
                            {


                                var componentName = dataGridView1.Rows[i].Cells[2].Value.ToString();

                                var componentDescription = dataGridView1.Rows[i].Cells[3].Value==null? "": dataGridView1.Rows[i].Cells[3].Value.ToString();
                                componentWeight = Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value.ToString());


                                if ((dataGridView1.Rows[i].Cells[5].Value != null &&
                                     (bool)dataGridView1.Rows[i].Cells[5].Value))
                                    isDominant = true;
                                else
                                    isDominant = false;

                                var result =
                                    PtcsEntities.Tbl_Component.Add(new Tbl_Component
                                    {
                                        ComponentId = Guid.NewGuid(),
                                        QuestionnaireDimensionId = QuestionnaireDimensionId,
                                        TechnologyId = TechnologyId,
                                        ComponentName = componentName,
                                        ComponentDescription = componentDescription,
                                        ComponentWeight = componentWeight,
                                        IsDominant = isDominant
                                    });
                                //db.Component_psave(QuestionnaireDimensionId, TechnologyId, componentName, componentDescription,
                                //                   componentWeight, isDominant).FirstOrDefault();

                                if (result != null)
                                {
                                    PtcsEntities.SaveChanges();
                                }
                                else
                                {
                                    XtraMessageBox.Show("خطا در ثبت مولفه", "خطا", MessageBoxButtons.OK,
                                             MessageBoxIcon.Error);
                                }
                            }
                            XtraMessageBox.Show("تغییرات پرسشنامه با موفقیت ثبت شد", "آگهی", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                        }
                    }

                    #endregion
                }
                else
                    XtraMessageBox.Show("لیست اطلاعات مولفه خالی می باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Warning);



            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {

            decimal currentweight = Convert.ToDecimal(e.Row.Cells[4].Value);
            decimal sumValue = currentweight / (dataGridView1.RowCount - 1);


            for (int i = 0; i <= dataGridView1.RowCount - 2; i++)
            {
                //if (dataGridView1.Rows[i].Index != dataGridView1.CurrentRow.Index)
                //{
                dataGridView1.Rows[i].Cells[4].Value = (Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value) + sumValue).ToString("#.###").PadLeft(5, '0');
                //}
            }





        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {

            var query = PtcsEntities.Tbl_Typist
                .Where(a => a.nb == _cNbTypist)
                .Select(a => a.access_inprog)
                .FirstOrDefault();

            if (query.Count() != 0)
            {

                if (query.Contains(",1-1-2,") || query.Contains("1-1-2,"))
                {

                    SaveFileDialog saveFile = new SaveFileDialog();
                    saveFile.Filter = "Excel Files(.xlsx)| *.xlsx";
                    saveFile.FilterIndex = 0;
                    saveFile.RestoreDirectory = true;
                    saveFile.FileName = txtTechnologyName.Text.Trim();
                    saveFile.Title = "خروجی پرسشنامه";
                    if (saveFile.ShowDialog() == DialogResult.OK)
                    {


                        Workbook workbook = new Workbook();
                        Worksheet worksheet1 = workbook.Worksheets[0];
                        worksheet1.DisplayRightToLeft = true;

                        Style headerStyle = workbook.Styles[workbook.Styles.Add()];
                        headerStyle.Font.Name = "Tahoma";
                        headerStyle.Font.Size = 14;
                        headerStyle.VerticalAlignment = TextAlignmentType.Center;
                        headerStyle.HorizontalAlignment = TextAlignmentType.Center;
                        headerStyle.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thick;
                        headerStyle.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thick;
                        headerStyle.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thick;
                        headerStyle.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thick;


                        Style textStyle = workbook.Styles[workbook.Styles.Add()];
                        textStyle.Font.Name = "Tahoma";
                        textStyle.Font.Size = 12;
                        textStyle.IsTextWrapped = true;
                        textStyle.VerticalAlignment = TextAlignmentType.Right;
                        textStyle.HorizontalAlignment = TextAlignmentType.Right;
                        textStyle.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thick;
                        textStyle.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thick;

                        Style opinionStyle = workbook.Styles[workbook.Styles.Add()];
                        opinionStyle.Font.Name = "Tahoma";
                        opinionStyle.Font.Size = 14;
                        opinionStyle.Font.IsBold = true;
                        opinionStyle.Pattern = BackgroundType.Solid;
                        opinionStyle.ForegroundColor = Color.FromArgb(245, 245, 245);
                        opinionStyle.VerticalAlignment = TextAlignmentType.Center;
                        opinionStyle.HorizontalAlignment = TextAlignmentType.Center;
                        opinionStyle.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thick;
                        opinionStyle.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thick;
                        opinionStyle.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;

                        Style stl4 = workbook.Styles[workbook.Styles.Add()];
                        stl4.Font.Name = "Tahoma";
                        stl4.Font.Size = 12;
                        stl4.IsTextWrapped = true;
                        stl4.VerticalAlignment = TextAlignmentType.Right;
                        stl4.HorizontalAlignment = TextAlignmentType.Right;
                        stl4.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thick;
                        stl4.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thick;
                        stl4.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thick;

                        Style stl5 = workbook.Styles[workbook.Styles.Add()];
                        stl5.Font.Name = "Tahoma";
                        stl5.Font.Size = 14;
                        stl5.IsTextWrapped = true;
                        stl5.VerticalAlignment = TextAlignmentType.Center;
                        stl5.HorizontalAlignment = TextAlignmentType.Center;
                        stl5.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thick;
                        stl5.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thick;
                        stl5.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thick;
                        stl5.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thick;

                        Style technologyInfoStyle = workbook.Styles[workbook.Styles.Add()];
                        technologyInfoStyle.Font.Name = "Tahoma";
                        technologyInfoStyle.Font.Size = 14;
                        technologyInfoStyle.Pattern = BackgroundType.Solid;
                        technologyInfoStyle.ForegroundColor = Color.FromArgb(245, 245, 245);
                        technologyInfoStyle.IsTextWrapped = true;
                        technologyInfoStyle.VerticalAlignment = TextAlignmentType.Right;
                        technologyInfoStyle.HorizontalAlignment = TextAlignmentType.Right;
                        technologyInfoStyle.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thick;
                        technologyInfoStyle.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thick;

                        worksheet1.Cells["A1"].PutValue(txtTechnologyName.Text.Trim());
                        worksheet1.Cells["A1"].SetStyle(technologyInfoStyle);

                        worksheet1.Cells["B1"].PutValue(txtTechnologyDescription.Text.Trim());
                        worksheet1.Cells["B1"].SetStyle(technologyInfoStyle);
                        worksheet1.Cells.Merge(0, 1, 1, 3);

                        worksheet1.Cells["A2"].PutValue("ابعاد");
                        worksheet1.Cells["A2"].SetStyle(stl5);

                        worksheet1.Cells["B2"].PutValue("مولفه ها");
                        worksheet1.Cells["B2"].SetStyle(stl5);

                        worksheet1.Cells["C2"].PutValue("تعریف");
                        worksheet1.Cells["C2"].SetStyle(stl5);

                        worksheet1.Cells["D2"].PutValue("نظر خبره");
                        worksheet1.Cells["D2"].SetStyle(stl5);

                        var comments = (from i in PtcsEntities.Tbl_Component
                                        join j in PtcsEntities.Tbl_Technology on i.TechnologyId equals j.TechnologyId
                                        where i.TechnologyId == TechnologyId
                                        select i)
                                        .OrderBy(item => item.AutoKey)
                                        .ToList();

                        var questionnaireDimensions = (from i in PtcsEntities.Tbl_Component
                                                       join j in PtcsEntities.Tbl_QuestionnaireDimension on i.QuestionnaireDimensionId equals
                                                       j.QuestionnaireDimensionId
                                                       where i.TechnologyId == TechnologyId
                                                       select j)
                                                       .Distinct()
                                                       .OrderBy(item => item.AutoKey)
                                                       .ToList();

                        int row = 2;
                        int ii = 3;
                        int jj = 3;
                        foreach (var rec in questionnaireDimensions)
                        {

                            var questionnaireDimensionCellNumber = "";
                            var commentNameCellNumber = "";
                            var commentDescriptionCellNumber = "";
                            var commentWeightCellNumber = "";
                            var opinionCellNumber = "";


                            var questionnaireDimensionCount =
                                comments.Count(
                                    item => item.QuestionnaireDimensionId == rec.QuestionnaireDimensionId);



                            worksheet1.Cells.Merge(row, 0, questionnaireDimensionCount, 1);
                            row += questionnaireDimensionCount;

                            questionnaireDimensionCellNumber = "A" + ii;

                            worksheet1.Cells[questionnaireDimensionCellNumber].PutValue(rec.QuestionnaireDimensionName);
                            worksheet1.Cells[questionnaireDimensionCellNumber].SetStyle(headerStyle);

                            ii += questionnaireDimensionCount;


                            var comment = comments
                                .Where(item => item.QuestionnaireDimensionId == rec.QuestionnaireDimensionId)
                                .Select(item => item);
                            foreach (var rec2 in comment)
                            {
                                commentNameCellNumber = "B" + jj;
                                commentDescriptionCellNumber = "C" + jj;
                                opinionCellNumber = "D" + jj;

                                if (rec2.IsDominant)
                                {
                                    worksheet1.Cells[commentNameCellNumber].PutValue(rec2.ComponentName);
                                    worksheet1.Cells[commentNameCellNumber].SetStyle(textStyle);
                                }
                                else
                                {
                                    worksheet1.Cells[commentNameCellNumber].PutValue(rec2.ComponentName);
                                    worksheet1.Cells[commentNameCellNumber].SetStyle(textStyle);
                                }

                                worksheet1.Cells[commentDescriptionCellNumber].PutValue(rec2.ComponentDescription);
                                worksheet1.Cells[commentDescriptionCellNumber].SetStyle(textStyle);

                                //worksheet1.Cells[commentWeightCellNumber].PutValue(rec2.ComponentWeight);
                                //worksheet1.Cells[commentWeightCellNumber].SetStyle(stl2);

                                worksheet1.Cells[opinionCellNumber].SetStyle(opinionStyle);

                                jj++;
                            }
                            //jj += questionnaireDimensionCount;

                            //var aa = worksheet1.Cells[commentNameCellNumber].GetStyle();
                            //if (worksheet1.Cells[commentNameCellNumber].GetStyle()==stl1)
                            //    worksheet1.Cells[commentNameCellNumber].SetStyle(stl5);
                            //else
                            worksheet1.Cells[commentNameCellNumber].SetStyle(stl4);
                            worksheet1.Cells[commentDescriptionCellNumber].SetStyle(stl4);
                            //worksheet1.Cells[commentWeightCellNumber].SetStyle(stl4);
                        }

                        int u = workbook.Worksheets.Add();
                        Worksheet worksheet2 = workbook.Worksheets[u];

                        DataTable dt = new DataTable();

                        dt.Columns.Add("Objects");
                        for (int j = 0; j <= 5; j++)
                            dt.Rows.Add(j.ToString());

                        worksheet2.Cells.ImportDataTable(dt, false, "A1");

                        int rowindex = worksheet2.Cells.MaxDataRow;
                        string cellname = CellsHelper.CellIndexToName(rowindex, 0);
                        Range range = worksheet2.Cells.CreateRange("A1", cellname);
                        // Name the range.
                        range.Name = "MyRange";


                        ValidationCollection validations = worksheet1.Validations;
                        Validation validation = validations[validations.Add()];
                        validation.Type = ValidationType.List;
                        validation.Operator = OperatorType.None;
                        validation.InCellDropDown = true;
                        validation.Formula1 = "=MyRange";
                        validation.ShowError = true;
                        validation.AlertStyle = ValidationAlertType.Stop;
                        validation.ErrorTitle = "خطا در نمایش آیتم ها";
                        validation.ErrorMessage = "لطفا یکی از آیتم ها را انتخاب نمایید";

                        CellArea area;
                        area.StartRow = 0;
                        area.EndRow = jj - 2;
                        area.StartColumn = 3;
                        area.EndColumn = 3;
                        validation.AreaList.Add(area);


                        worksheet2.Cells["A1000"].PutValue(TechnologyId.ToString());


                        ProtectedRangeCollection allowRanges = worksheet1.AllowEditRanges;
                        ProtectedRange protecedRange;
                        int idx = allowRanges.Add("name", 1, 3, jj - 2, 3);
                        protecedRange = allowRanges[idx];
                        //proteced_range.Password = "123";
                        worksheet1.Protect(ProtectionType.All);

                        workbook.Worksheets[0].Cells.SetColumnWidth(0, 30);
                        workbook.Worksheets[0].Cells.SetColumnWidth(1, 60);
                        workbook.Worksheets[0].Cells.SetColumnWidth(2, 90);
                        workbook.Worksheets[0].Cells.SetColumnWidth(3, 20);
                        workbook.Worksheets[0].AutoFitRow(1, jj - 2, 1, 2);
                        workbook.Save(saveFile.FileName);
                    }
                }
                else
                    XtraMessageBox.Show("کاربر محترم شما مجوز گرفتن خروجی Excel را ندارید", "آگهی",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (_componentStatus)
            {
                _componentStatus = false;
                proc_Component();
            }
            else
            {
                _componentStatus = true;
                proc_Component();
            }
        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell is DataGridViewCheckBoxCell)
            {
                DataGridViewCheckBoxCell cb = (DataGridViewCheckBoxCell)dataGridView1.CurrentCell;

                var isdominet = cb.Value;
                if (isdominet == "T")
                {
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        dataGridView1.Rows[i].Cells[5].Value = false;
                    }
                    dataGridView1.CurrentRow.Cells[5].Value = true;
                }
                else if (isdominet == "F")
                {
                    dataGridView1.CurrentRow.Cells[5].Value = false;
                }
                else
                {
                    if (dataGridView1.CurrentRow.Cells[5].Value != null &&
                        (bool)dataGridView1.CurrentRow.Cells[5].Value)
                    {
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            dataGridView1.Rows[i].Cells[5].Value = false;
                        }
                        dataGridView1.CurrentRow.Cells[5].Value = true;
                    }
                }
                dataGridView1.EndEdit();
            }

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                decimal _weight;
                List<Weights> weights = new List<Weights>();
                if (dataGridView1.CurrentRow != null && (e.RowIndex == dataGridView1.CurrentRow.Index && e.ColumnIndex == 4))
                {
                    if (dataGridView1.CurrentRow.Cells[0].Value!=null)//حالت ویرایش
                    {
                        if (Convert.ToDecimal(dataGridView1.CurrentRow.Cells[4].Value) > 1)
                            dataGridView1.CurrentRow.Cells[3].Value = 0;
                        else
                        {
                            var componentId = Guid.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                            var _OldComponentWeight = PtcsEntities.Tbl_Component
                                .Where(a => a.ComponentId == componentId)
                                .Select(a => a.ComponentWeight)
                                .FirstOrDefault();

                            var _diffrence = Convert.ToDecimal(dataGridView1.CurrentRow.Cells[4].Value.ToString()) -
                                             _OldComponentWeight;
                            var differenceWeigth =
                                Math.Round(_diffrence / (dataGridView1.RowCount - 2), 3);
                            weights.Clear();
                            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                            {
                                if (dataGridView1.Rows[i].Cells[4].Value != dataGridView1.CurrentRow.Cells[4].Value)
                                {
                                    _weight = Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value) - differenceWeigth;

                                    if (_weight > 0)
                                    {
                                        weights.Add(new Weights
                                        {
                                            Weigth = _weight
                                        });
                                    }
                                }
                                else
                                {
                                    
                                        weights.Add(new Weights
                                        {
                                            Weigth = Convert.ToDecimal(dataGridView1.CurrentRow.Cells[4].Value.ToString())
                                        });
                                   
                                }
                            }
                            if (weights.Count == (dataGridView1.RowCount - 1))
                            {
                                for (int i = 0; i < weights.Count; i++)
                                {
                                    dataGridView1.Rows[i].Cells[4].Value = Math.Round(weights[i].Weigth, 3);
                                }
                            }
                        }
                    }
                    else
                    {//حالت ایجاد
                       
                       
                        if (dataGridView1.CurrentRow.Cells[4].Value != null)
                        {
                            if (Convert.ToDecimal(dataGridView1.CurrentRow.Cells[4].Value) > 1)
                                dataGridView1.CurrentRow.Cells[3].Value = 0;
                            else
                            {
                                var differenceWeigth =
                                    Math.Round(Convert.ToDecimal(dataGridView1.CurrentRow.Cells[4].Value.ToString()) /
                                               (dataGridView1.RowCount - 2), 3);

                                for (int i = 0; i < dataGridView1.Rows.Count - 2; i++)
                                {
                                    _weight = Convert.ToDecimal(dataGridView1.Rows[i].Cells[4].Value) - differenceWeigth;

                                    if (_weight > 0)
                                    {
                                        weights.Add(new Weights
                                        {
                                            Weigth = _weight
                                        });
                                    }

                                    else
                                    {
                                        dataGridView1.CurrentRow.Cells[4].Value = "0";
                                        weights.Clear();
                                        return;
                                    }
                                }

                                if (weights.Count == (dataGridView1.RowCount - 2))
                                {
                                    for (int i = 0; i < weights.Count; i++)
                                    {
                                        dataGridView1.Rows[i].Cells[4].Value = Math.Round(weights[i].Weigth, 3);
                                    }
                                }

                                //for (int i = 0; i < dataGridView1.Rows.Count; i++)
                                //{
                                //    weight += Convert.ToDecimal(dataGridView1.Rows[i].Cells[3].Value);
                                //}
                                //if (weight > 1)
                                //{
                                //    dataGridView1.CurrentRow.Cells[3].Value = 0;
                                //}
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                if (dataGridView1.CurrentRow != null) dataGridView1.CurrentRow.Cells[4].Value = 0;
            }
        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FrmTechnologySearch a = new FrmTechnologySearch();

            a.ShowDialog();
            if (a.Ss != Guid.Empty)
            {
                TechnologyId = a.Ss;
                proc_refresh();
            }
        }
    }
}
