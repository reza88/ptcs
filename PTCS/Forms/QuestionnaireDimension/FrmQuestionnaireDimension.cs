﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS.Forms.QuestionnaireDimension
{
    public partial class FrmQuestionnaireDimension : XtraForm
    {
        public PTCSEntities PtcsEntities;
        public FrmQuestionnaireDimension()
        {
            var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
            PtcsEntities = new PTCSEntities();
            PtcsEntities.Database.Connection.ConnectionString = strCnnString;
            InitializeComponent();
        }



        private void FrmQuestionnaireDimension_Load(object sender, EventArgs e)
        {
            var query = PtcsEntities.Tbl_QuestionnaireDimension
                .Select(i => i)
                .OrderBy(i => i.AutoKey)
                .ToList();
            if (query.Count() != 0)
            {
                foreach (var rec in query)
                {
                    int n = dataGridView1.Rows.Add();
                    dataGridView1.Rows[n].Cells[0].Value = rec.QuestionnaireDimensionId;
                    dataGridView1.Rows[n].Cells[1].Value = rec.QuestionnaireDimensionName;
               }
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           try
            {
                if (dataGridView1.RowCount > 0)
                {
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        var questionnaireDimensionId =Guid.Parse(dataGridView1.Rows[i].Cells[0].Value.ToString());
                        var query = PtcsEntities.Tbl_QuestionnaireDimension
                            .Where(a =>a.QuestionnaireDimensionId == questionnaireDimensionId)
                            .Select(a => a);
                        if (query.Count() != 0)
                        {
                            query.FirstOrDefault().QuestionnaireDimensionName =
                                dataGridView1.Rows[i].Cells[1].Value.ToString();
                        }

                        PtcsEntities.SaveChanges();
                    }
                    XtraMessageBox.Show("ابعاد پرسشنامه با موفقیت ویرایش شد", "آگهی", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                XtraMessageBox.Show("مشکل در ثبت ابعاد پرسشنامه", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }
    }
}
