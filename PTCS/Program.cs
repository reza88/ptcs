﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using PTCS.Aspose;
using PTCSModel;
using PTCSModel.Classes;

namespace PTCS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //UserLookAndFeel.Default.SetSkinStyle("Office 2010 Silver");
            //UserLookAndFeel.Default.SetSkinStyle("Office 2010 Blue");
            //UserLookAndFeel.Default.SetSkinStyle("Office 2010 Black");
            //UserLookAndFeel.Default.SetSkinStyle("Visual Studio 2010");
            //UserLookAndFeel.Default.SetSkinStyle("Seven Classic");
            //UserLookAndFeel.Default.SetSkinStyle("Office 2016 Black");
            //UserLookAndFeel.Default.SetSkinStyle("Office 2013 White");
            //UserLookAndFeel.Default.SetSkinStyle("Office 2013 Dark Gray");
            UserLookAndFeel.Default.SetSkinStyle("Office 2013 Light Gray");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ModifyInMemory.ActivateMemoryPatching();

            Thread spThread = new Thread(() => new Login.FrmLoading().ShowDialog());

            spThread.Start();

            Thread.Sleep(3000);
            spThread.Abort();

            try
            {
                var strCnnString = Functions.GetConnectString(AppDomain.CurrentDomain.BaseDirectory + @"data\PTCSData.sdf", "ppk123456");
                PTCSEntities ptcsEntities = new PTCSEntities();
                ptcsEntities.Database.Connection.ConnectionString = strCnnString;


                //Application.Run(new Login.FrmMain(1));
                Application.Run(new Login.FrmLogin());
                //Application.Run(new Form100());

            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }


    }
}
