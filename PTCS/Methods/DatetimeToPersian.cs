﻿using System;

namespace PTCS.Methods
{
    public class DatetimeToPersian
    {
        public string proc_datetime_persian(DateTime datetimeMiladi)
        {
            System.Globalization.PersianCalendar pc = new System.Globalization.PersianCalendar();
            string dtPer = pc.GetYear(datetimeMiladi).ToString().Substring(0, 4) + "/" +
                            pc.GetMonth(datetimeMiladi).ToString().PadLeft(2, '0') + "/" +
                            pc.GetDayOfMonth(datetimeMiladi).ToString().PadLeft(2, '0') + " " +
                            pc.GetHour(datetimeMiladi).ToString().PadLeft(2, '0') + ":" +
                            pc.GetMinute(datetimeMiladi).ToString().PadLeft(2, '0') + ":" +
                            pc.GetSecond(datetimeMiladi).ToString().PadLeft(2, '0');
            return dtPer;
        }
    }
}
