﻿using System;

namespace PTCS.Methods
{
    public class EnumGuid : Attribute
    {
        public Guid Guid;


        public EnumGuid(string guid)
        {
            Guid = new Guid(guid);
        }
      
    }

    public class IntEnum : Attribute
    {
        public int Intenum;
        public int Enumint(string guid)
        {
            Intenum = int.Parse(guid);
            return Intenum;
        }
    }

    

}

