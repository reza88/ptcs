﻿using System.ComponentModel;

namespace PTCS.Methods
{
    public enum QuestionnaireDimensions 
    {
        [Description("مشخصات فنی")]
        [EnumGuid("efc0abd9-5ff6-4249-9d1d-2ac08cec7613")]
        TechnicalSpecifications =1,
        [Description("مشخصات مالی و اقتصادی")]
        [EnumGuid("3172cb58-8bde-4f10-8ced-7943489790ea")]
        FinancialAndEconomicData = 2,
        [Description("قوانین و تائیدیه ها")]
        [EnumGuid("4d6a08ca-6ad7-400b-99cc-7d8833dc30ab")]
        RulesAndApprovals = 3,
        [Description("مشخصات بازار")]
        [EnumGuid("62250728-e71f-4f20-85dd-981a25a6dd50")]
        MarketSpecifications = 4,


    }
}
